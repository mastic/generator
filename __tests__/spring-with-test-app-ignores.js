"use strict";

const assert = require("yeoman-assert");
const { runStackTest } = require("./testRunner");

describe("spring-with-test-app-ignores", () => {
  beforeAll(() => runStackTest("test-app-spring-ignores", "spring", []));

  it(".masticignore exists", () => {
    assert.file([".masticignore"]);
  });

  it("README.md exists", () => {
    assert.file(["README.md"]);
  });

  it("README.md is ignored", () => {
    assert.fileContent("README.md", "I am ignored.");
  });
});
