"use strict";
const path = require("path");
const assert = require("yeoman-assert");
const fs = require("fs-extra");
const { readModel, writeModel } = require("../generators/model-utils");

describe("model-utils", () => {
  it(".readModel() reads model", () => {
    const dir = path.join(__dirname, "./model-test.yml");
    const model = readModel(fs, dir);
    assert.ok(model);
    assert.ok(model.entities);
    assert.ok(model.enums);
  });

  it(".writeModel() writes model", () => {
    fs.mkdirpSync("temp");
    const tmpdir = fs.mkdtempSync("temp/test");
    const dir = path.join(tmpdir, "./model-test.yml");
    const model = {
      entities: [],
      enums: [],
    };
    writeModel(fs, dir, model);
    const modelRestored = readModel(fs, dir);
    assert.ok(modelRestored);
    assert.deepStrictEqual(model, modelRestored);
  });

  it(".readModel() reads enum field", () => {
    const dir = path.join(__dirname, "./model-test.yml");
    const model = readModel(fs, dir);
    const testEntity = model.entities[0];
    assert.ok(testEntity);
    assert.ok(testEntity.name);
    assert.ok(testEntity.fields);
    assert.strictEqual("TestEntity", testEntity.name);

    const enumField = testEntity.fields.filter(
      (field) => field.name === "enumField"
    )[0];
    const stringField = testEntity.fields.filter(
      (field) => field.name === "stringField"
    )[0];
    const booleanField = testEntity.fields.filter(
      (field) => field.name === "booleanField"
    )[0];
    const integerField = testEntity.fields.filter(
      (field) => field.name === "integerField"
    )[0];
    const longField = testEntity.fields.filter(
      (field) => field.name === "longField"
    )[0];
    const doubleField = testEntity.fields.filter(
      (field) => field.name === "doubleField"
    )[0];
    const datetimeField = testEntity.fields.filter(
      (field) => field.name === "datetimeField"
    )[0];

    assert.ok(enumField);
    assert.strictEqual("SomeType", enumField.type);
    assert.strictEqual(true, enumField.isEnum());

    assert.ok(stringField);
    assert.strictEqual("String", stringField.type);
    assert.strictEqual(false, stringField.isEnum());

    assert.ok(booleanField);
    assert.strictEqual("bool", booleanField.type);
    assert.strictEqual(false, booleanField.isEnum());

    assert.ok(integerField);
    assert.strictEqual("int", integerField.type);
    assert.strictEqual(false, integerField.isEnum());

    assert.ok(longField);
    assert.strictEqual("long", longField.type);
    assert.strictEqual(false, longField.isEnum());

    assert.ok(doubleField);
    assert.strictEqual("double", doubleField.type);
    assert.strictEqual(false, doubleField.isEnum());

    assert.ok(datetimeField);
    assert.strictEqual("datetime", datetimeField.type);
    assert.strictEqual(false, datetimeField.isEnum());
  });

  it(".readModel() reads enum", () => {
    const dir = path.join(__dirname, "./model-test.yml");
    const model = readModel(fs, dir);
    const testEnum = model.enums[0];
    assert.ok(testEnum);
    assert.ok(testEnum.name);
    assert.ok(testEnum.values);
    assert.strictEqual("SomeType", testEnum.name);
    assert.strictEqual(3, testEnum.values.length);
  });

  it("enumField.isEnum() returns true", () => {
    const dir = path.join(__dirname, "./model-test.yml");
    const model = readModel(fs, dir);

    const testEntity = model.entities[0];
    const enumField = testEntity.fields.filter(
      (field) => field.name === "enumField"
    )[0];

    assert.ok(enumField);
    assert.strictEqual(true, enumField.isEnum());
  });

  it("stringField.isEnum() returns false", () => {
    const dir = path.join(__dirname, "./model-test.yml");
    const model = readModel(fs, dir);

    const testEntity = model.entities[0];
    const stringField = testEntity.fields.filter(
      (field) => field.name === "stringField"
    )[0];

    assert.ok(stringField);
    assert.strictEqual(false, stringField.isEnum());
  });

  it("enumField.getEnum() returns Enum type (SomeType)", () => {
    const dir = path.join(__dirname, "./model-test.yml");
    const model = readModel(fs, dir);

    const testEntity = model.entities[0];
    const enumField = testEntity.fields.filter(
      (field) => field.name === "enumField"
    )[0];

    assert.ok(enumField);
    assert.ok(enumField.getEnum());
    assert.strictEqual("SomeType", enumField.getEnum().name);
  });

  it("stringField.getEnum() is undefined", () => {
    const dir = path.join(__dirname, "./model-test.yml");
    const model = readModel(fs, dir);

    const testEntity = model.entities[0];
    const stringField = testEntity.fields.filter(
      (field) => field.name === "stringField"
    )[0];

    assert.ok(stringField);
    assert.strictEqual(undefined, stringField.getEnum());
  });

  it("enumField.enum returns Enum type (SomeType)", () => {
    const dir = path.join(__dirname, "./model-test.yml");
    const model = readModel(fs, dir);

    const testEntity = model.entities[0];
    const enumField = testEntity.fields.filter(
      (field) => field.name === "enumField"
    )[0];

    assert.ok(enumField);
    assert.ok(enumField.enum);
    assert.strictEqual("SomeType", enumField.enum.name);
  });

  it("stringField.enum is undefined", () => {
    const dir = path.join(__dirname, "./model-test.yml");
    const model = readModel(fs, dir);

    const testEntity = model.entities[0];
    const stringField = testEntity.fields.filter(
      (field) => field.name === "stringField"
    )[0];

    assert.ok(stringField);
    assert.strictEqual(undefined, stringField.enum);
  });

  it("invalid visitor throws exception", () => {
    const dir = path.join(__dirname, "./model-test.yml");
    const model = readModel(fs, dir);

    const testEntity = model.entities[0];
    const longField = testEntity.fields.filter(
      (field) => field.name === "longField"
    )[0];

    assert.throws(() => longField.accept(undefined), Error);
    assert.throws(() => longField.accept({}), Error);
  });

  it("typeEntity relation", () => {
    const dir = path.join(__dirname, "./model-test.yml");
    const model = readModel(fs, dir);

    const entity1 = model.entities[1];
    const nameField = entity1.fields.filter(
      (field) => field.name === "name"
    )[0];
    const relationField = entity1.fields.filter(
      (field) => field.name === "testEntity"
    )[0];

    assert.strictEqual(undefined, nameField.getTypeEntity());
    assert.strictEqual(undefined, nameField.typeEntity);
    assert.strictEqual("TestEntity", relationField.getTypeEntity().name);
    assert.strictEqual("TestEntity", relationField.typeEntity.name);
  });

  it("stringField.isOptional() returns false", () => {
    const dir = path.join(__dirname, "./model-test.yml");
    const model = readModel(fs, dir);

    const testEntity = model.entities[0];
    const stringField = testEntity.fields.filter(
      (field) => field.name === "stringField"
    )[0];

    assert.ok(stringField);
    assert.strictEqual(false, stringField.isOptional());
  });

  it("stringField.optional is not defined", () => {
    const dir = path.join(__dirname, "./model-test.yml");
    const model = readModel(fs, dir);

    const testEntity = model.entities[0];
    const stringField = testEntity.fields.filter(
      (field) => field.name === "stringField"
    )[0];

    assert.ok(stringField);
    assert.strictEqual(undefined, stringField.optional);
  });

  it(".isOptional() returns true on optional fields", () => {
    const dir = path.join(__dirname, "./model-test.yml");
    const model = readModel(fs, dir);

    const testEntity = model.entities[0];
    const datetimeFieldOptional = testEntity.fields.filter(
      (field) => field.name === "datetimeFieldOptional"
    )[0];
    const enumFieldOptional = testEntity.fields.filter(
      (field) => field.name === "enumFieldOptional"
    )[0];

    assert.ok(datetimeFieldOptional);
    assert.ok(enumFieldOptional);
    assert.strictEqual(true, enumFieldOptional.isOptional());
    assert.strictEqual(true, datetimeFieldOptional.isOptional());
  });

  it(".optional is true on optional fields", () => {
    const dir = path.join(__dirname, "./model-test.yml");
    const model = readModel(fs, dir);

    const testEntity = model.entities[0];
    const datetimeFieldOptional = testEntity.fields.filter(
      (field) => field.name === "datetimeFieldOptional"
    )[0];
    const enumFieldOptional = testEntity.fields.filter(
      (field) => field.name === "enumFieldOptional"
    )[0];

    assert.ok(datetimeFieldOptional);
    assert.ok(enumFieldOptional);
    assert.strictEqual(true, enumFieldOptional.optional);
    assert.strictEqual(true, datetimeFieldOptional.optional);
  });
});
