"use strict";
const path = require("path");
const assert = require("yeoman-assert");
const fs = require("fs-extra");
const { readModel } = require("../generators/model-utils");
const {
  properties,
  fromRow,
  toRow,
} = require("../generators/quarkus/entity-utils");

describe("quarkus-entity-utils", () => {
  const dir = path.join(__dirname, "./model-test.yml");
  const model = readModel(fs, dir);
  let enumField;
  let stringField;
  let booleanField;
  let longField;
  let integerField;
  let doubleField;
  let datetimeField;

  beforeAll(() => {
    const file = path.join(__dirname, "./model-test.yml");
    const model = readModel(fs, file);
    const entity = model.entities.filter(
      (entity) => entity.name === "TestEntity"
    )[0];
    enumField = entity.fields.filter((field) => field.name === "enumField")[0];
    stringField = entity.fields.filter(
      (field) => field.name === "stringField"
    )[0];
    booleanField = entity.fields.filter(
      (field) => field.name === "booleanField"
    )[0];
    longField = entity.fields.filter((field) => field.name === "longField")[0];
    integerField = entity.fields.filter(
      (field) => field.name === "integerField"
    )[0];
    doubleField = entity.fields.filter(
      (field) => field.name === "doubleField"
    )[0];
    datetimeField = entity.fields.filter(
      (field) => field.name === "datetimeField"
    )[0];
  });

  it(".properties() with empty properties", () => {
    const entity = model.entities[0];

    assert.ok(properties(entity, undefined));
    assert.ok(properties(entity, {}));

    assert.strictEqual(
      JSON.stringify(properties(entity, undefined)),
      JSON.stringify(properties(entity, {}))
    );
  });

  it(".fromRow()", () => {
    assert.strictEqual(
      fromRow(enumField),
      `SomeType.valueOf(row.getString("enum_field"))`
    );
    assert.strictEqual(fromRow(stringField), `row.getString("string_field")`);
    assert.strictEqual(
      fromRow(booleanField),
      `row.getBoolean("boolean_field")`
    );
    assert.strictEqual(fromRow(longField), `row.getLong("long_field")`);
    assert.strictEqual(
      fromRow(integerField),
      `row.getInteger("integer_field")`
    );
    assert.strictEqual(fromRow(doubleField), `row.getDouble("double_field")`);
    assert.strictEqual(
      fromRow(datetimeField),
      `row.getLocalDateTime("datetime_field").atZone(UTC).toInstant()`
    );
  });

  it(".toRow()", () => {
    assert.strictEqual(
      toRow(enumField),
      `.addString(entity.enumField.toString())`
    );
    assert.strictEqual(toRow(stringField), `.addString(entity.stringField)`);
    assert.strictEqual(toRow(booleanField), `.addBoolean(entity.booleanField)`);
    assert.strictEqual(toRow(longField), `.addLong(entity.longField)`);
    assert.strictEqual(toRow(integerField), `.addInteger(entity.integerField)`);
    assert.strictEqual(toRow(doubleField), `.addDouble(entity.doubleField)`);
    assert.strictEqual(
      toRow(datetimeField),
      `.addLocalDateTime(LocalDateTime.ofInstant(entity.datetimeField, UTC))`
    );
  });
});
