"use strict";
const path = require("path");
const fs = require("fs-extra");
const assert = require("yeoman-assert");
const helpers = require("yeoman-test");

describe("generator-mastic:spring-reactive-postgresql", () => {
  beforeAll(() =>
    helpers
      .run(path.join(__dirname, "../generators/spring-reactive-postgresql"))
      .inTmpDir((dir) => {
        fs.copySync(path.join(__dirname, "test-app-spring-postgresql"), dir);
      })
  );

  it("creates files", () => {
    assert.file([
      "docker-compose.yml",
      "src/main/java/com/example/db/DatabaseConfiguration.java",
      "src/main/java/com/example/db/DatabaseMigrationProperties.java",
      "src/main/java/com/example/exception/ExceptionHandler.java",
      "src/main/java/com/example/exception/NotFoundException.java",
      "src/main/java/com/example/util/FluxUtils.java",
      "src/main/java/com/example/Fruit.java",
      "src/main/java/com/example/FruitRepository.java",
      "src/main/java/com/example/FruitController.java",
      "src/main/java/com/example/SomeEntity.java",
      "src/main/java/com/example/SomeEntityRepository.java",
      "src/main/java/com/example/SomeEntityController.java",
      "src/test/java/com/example/FruitControllerTest.java",
      "src/test/java/com/example/SomeEntityControllerTest.java",
    ]);
  });
});
