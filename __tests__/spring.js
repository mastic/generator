"use strict";
const path = require("path");
const assert = require("yeoman-assert");
const helpers = require("yeoman-test");

describe("generator-mastic:spring", () => {
  beforeAll(() => helpers.run(path.join(__dirname, "../generators/spring")));

  it("creates files", () => {
    assert.file([
      ".dockerignore",
      ".gitignore",
      "README.md",
      "build.gradle",
      "settings.gradle",
      "build.gradle",
      "gradlew.bat",
      "gradlew",
      "gradle/wrapper/gradle-wrapper.properties",
      "gradle/wrapper/gradle-wrapper.jar",
      "src/main/resources/application.yml",
      "src/test/resources/application-test.yml",
    ]);
  });
});
