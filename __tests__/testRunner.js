"use strict";
const path = require("path");
const fs = require("fs-extra");
const helpers = require("yeoman-test");

// eslint-disable-next-line no-unused-vars
function ls(msg, dir) {
  console.log(`${msg.padEnd(30)}: ${fs.readdirSync(dir)}`);
}

function runAspects(resultDir, aspects) {
  if (!aspects || aspects.length === 0) {
    return () => resultDir;
  }

  const aspect = aspects[0];
  return helpers
    .run(path.join(__dirname, `../generators/${aspect}`))
    .inTmpDir((tmpdir) => {
      fs.copySync(resultDir, tmpdir);
    })
    .then((result) => {
      const nextAspects = aspects.slice(1);
      return runAspects(result, nextAspects);
    });
}

function runStackTest(appDir, stack, aspects) {
  if (!aspects) aspects = [];
  return helpers
    .run(path.join(__dirname, `../generators/${stack}`))
    .inTmpDir((tmpdir) => {
      const srcdir = path.join(__dirname, appDir);
      fs.copySync(srcdir, tmpdir);
    })
    .then((result) => runAspects(result, aspects));
}

module.exports = {
  runStackTest,
};
