"use strict";
const path = require("path");
const fs = require("fs-extra");
const assert = require("yeoman-assert");
const helpers = require("yeoman-test");

describe("generator-mastic:quarkus-liquibase", () => {
  beforeAll(() =>
    helpers
      .run(path.join(__dirname, "../generators/quarkus-liquibase"))
      .inTmpDir((dir) => {
        fs.copySync(path.join(__dirname, "test-app-quarkus-postgresql"), dir);
      })
  );

  it("creates files", () => {
    assert.file([
      "src/main/resources/db/changeLog.xml",
      "src/main/resources/db/initial_schema.xml",
      "src/main/resources/db/entity_Fruit.xml",
      "src/main/resources/db/entity_SomeEntity.xml",
    ]);

    assert.fileContent([
      [
        "src/main/resources/db/changeLog.xml",
        `<include file="db/entity_Fruit.xml" relativeToChangelogFile="false"/>`,
      ],
      [
        "src/main/resources/db/changeLog.xml",
        `<include file="db/entity_SomeEntity.xml" relativeToChangelogFile="false"/>`,
      ],
    ]);
  });
});
