"use strict";
const path = require("path");
const assert = require("yeoman-assert");
const fs = require("fs-extra");
const { readModel } = require("../generators/model-utils");
const { properties } = require("../generators/enum-utils");

describe("spring-entity-utils", () => {
  const dir = path.join(__dirname, "./model-test.yml");
  const model = readModel(fs, dir);

  it(".properties() with empty properties", () => {
    const entity = model.entities[0];

    assert.ok(properties(entity, undefined));
    assert.ok(properties(entity, {}));

    assert.strictEqual(
      JSON.stringify(properties(entity, undefined)),
      JSON.stringify(properties(entity, {}))
    );
  });
});
