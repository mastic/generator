"use strict";

const assert = require("yeoman-assert");
const { runStackTest } = require("./testRunner");

describe("angular-layout-with-test-app", () => {
  beforeAll(() =>
    runStackTest("test-app-angular-layout", "angular", [
      "angular-layout",
      "angular-entity",
    ])
  );

  it("angular stack creates files", () => {
    assert.file([
      ".browserslistrc",
      ".editorconfig",
      ".eslintrc.json",
      ".gitignore",
      "angular.json",
      "proxy.conf.json",
      "karma.conf.js",
      "package.json",
      "README.md",
      "src/app/app.component.html",
      "src/app/app.component.sass",
      "src/app/app.component.spec.ts",
      "src/app/app.component.ts",
      "src/app/app.module.ts",
      "src/app/app-routing.module.ts",
      "src/assets/.gitkeep",
      "src/environments/environment.prod.ts",
      "src/environments/environment.ts",
      "src/favicon.ico",
      "src/index.html",
      "src/main.ts",
      "src/polyfills.ts",
      "src/styles.sass",
      "src/test.ts",
      "tsconfig.app.json",
      "tsconfig.json",
      "tsconfig.spec.json",
    ]);
    assert.fileContent([
      ["README.md", `# Sample`],
      ["src/index.html", `<title>Sample</title>`],
    ]);
  });

  it("angular-layout aspect creates files", () => {
    assert.file([
      "src/app/elevate.directive.spec.ts",
      "src/app/elevate.directive.ts",
      "src/app/error/page-not-found.component.html",
      "src/app/error/page-not-found.component.sass",
      "src/app/error/page-not-found.component.spec.ts",
      "src/app/error/page-not-found.component.ts",
      "src/app/header/header.component.html",
      "src/app/header/header.component.sass",
      "src/app/header/header.component.spec.ts",
      "src/app/header/header.component.ts",
      "src/app/side-navigation/side-navigation.component.html",
      "src/app/side-navigation/side-navigation.component.sass",
      "src/app/side-navigation/side-navigation.component.spec.ts",
      "src/app/side-navigation/side-navigation.component.ts",
      "src/app/welcome/welcome.component.html",
      "src/app/welcome/welcome.component.sass",
      "src/app/welcome/welcome.component.spec.ts",
      "src/app/welcome/welcome.component.ts",
    ]);
  });

  it("angular-entity creates files", () => {
    assert.file([
      "src/app/fruit/fruit.actions.ts",
      "src/app/fruit/fruit.component.html",
      "src/app/fruit/fruit.component.sass",
      "src/app/fruit/fruit.component.spec.ts",
      "src/app/fruit/fruit.component.ts",
      "src/app/fruit/fruit-delete-dialog.component.html",
      "src/app/fruit/fruit-delete-dialog.component.sass",
      "src/app/fruit/fruit-delete-dialog.component.spec.ts",
      "src/app/fruit/fruit-delete-dialog.component.ts",
      "src/app/fruit/fruit.effects.spec.ts",
      "src/app/fruit/fruit.effects.ts",
      "src/app/fruit/fruit-form.component.html",
      "src/app/fruit/fruit-form.component.sass",
      "src/app/fruit/fruit-form.component.spec.ts",
      "src/app/fruit/fruit-form.component.ts",
      "src/app/fruit/fruit.model.ts",
      "src/app/fruit/fruit.module.ts",
      "src/app/fruit/fruit.reducer.spec.ts",
      "src/app/fruit/fruit.reducer.ts",
      "src/app/fruit/fruit.selectors.spec.ts",
      "src/app/fruit/fruit.selectors.ts",
      "src/app/fruit/fruit.service.spec.ts",
      "src/app/fruit/fruit.service.ts",
      "src/app/fruit/fruit-update.component.html",
      "src/app/fruit/fruit-update.component.sass",
      "src/app/fruit/fruit-update.component.spec.ts",
      "src/app/fruit/fruit-update.component.ts",
      "src/app/some-entity/some-entity.actions.ts",
      "src/app/some-entity/some-entity.component.html",
      "src/app/some-entity/some-entity.component.sass",
      "src/app/some-entity/some-entity.component.spec.ts",
      "src/app/some-entity/some-entity.component.ts",
      "src/app/some-entity/some-entity-delete-dialog.component.html",
      "src/app/some-entity/some-entity-delete-dialog.component.sass",
      "src/app/some-entity/some-entity-delete-dialog.component.spec.ts",
      "src/app/some-entity/some-entity-delete-dialog.component.ts",
      "src/app/some-entity/some-entity.effects.spec.ts",
      "src/app/some-entity/some-entity.effects.ts",
      "src/app/some-entity/some-entity-form.component.html",
      "src/app/some-entity/some-entity-form.component.sass",
      "src/app/some-entity/some-entity-form.component.spec.ts",
      "src/app/some-entity/some-entity-form.component.ts",
      "src/app/some-entity/some-entity.model.ts",
      "src/app/some-type/some-type.ts",
      "src/app/some-entity/some-entity.module.ts",
      "src/app/some-entity/some-entity.reducer.spec.ts",
      "src/app/some-entity/some-entity.reducer.ts",
      "src/app/some-entity/some-entity.selectors.spec.ts",
      "src/app/some-entity/some-entity.selectors.ts",
      "src/app/some-entity/some-entity.service.spec.ts",
      "src/app/some-entity/some-entity.service.ts",
      "src/app/some-entity/some-entity-update.component.html",
      "src/app/some-entity/some-entity-update.component.sass",
      "src/app/some-entity/some-entity-update.component.spec.ts",
      "src/app/some-entity/some-entity-update.component.ts",
    ]);
  });

  it("side-navigation.component.html has entity references", () => {
    assert.fileContent([
      [
        "src/app/side-navigation/side-navigation.component.html",
        `<a mat-list-item [routerLink]="['/some-entity']">
    <mat-icon aria-hidden="false" class="right-space">article</mat-icon>
    Some Entity
  </a>`,
      ],
      [
        "src/app/side-navigation/side-navigation.component.html",
        `<a mat-list-item [routerLink]="['/fruit']">
    <mat-icon aria-hidden="false" class="right-space">article</mat-icon>
    Fruit
  </a>`,
      ],
    ]);
  });

  it("welcome.component.html has entity references", () => {
    assert.fileContent([
      [
        "src/app/welcome/welcome.component.html",
        `    <button [routerLink]="['/some-entity']" mat-raised-button appElevate class="entity-button">
      <mat-icon aria-hidden="false" class="right-space">article</mat-icon>
      Some Entity
    </button>`,
      ],
      [
        "src/app/welcome/welcome.component.html",
        `    <button [routerLink]="['/fruit']" mat-raised-button appElevate class="entity-button">
      <mat-icon aria-hidden="false" class="right-space">article</mat-icon>
      Fruit
    </button>`,
      ],
    ]);
  });

  it("has @angular/flex-layout dependency", () => {
    assert.fileContent([["package.json", /"@angular\/flex-layout": ".*"/]]);
  });

  it("has @angular/cdk dependency", () => {
    assert.fileContent([["package.json", /"@angular\/cdk": ".*"/]]);
  });

  it("has @ngrx/effects dependency", () => {
    assert.fileContent([["package.json", /"@ngrx\/effects": ".*"/]]);
  });

  it("has @ngrx/entity dependency", () => {
    assert.fileContent([["package.json", /"@ngrx\/entity": ".*"/]]);
  });

  it("has @ngrx/store dependency", () => {
    assert.fileContent([["package.json", /"@ngrx\/store": ".*"/]]);
  });

  it("has @ngrx/store-devtools dependency", () => {
    assert.fileContent([["package.json", /"@ngrx\/store-devtools": ".*"/]]);
  });

  it("has @ngrx/schematics dependency", () => {
    assert.fileContent([["package.json", /"@ngrx\/schematics": ".*"/]]);
  });

  it("has @types/uuid dependency", () => {
    assert.fileContent([["package.json", /"@types\/uuid": ".*"/]]);
  });

  it("has jasmine-marbles dependency", () => {
    assert.fileContent([["package.json", /"jasmine-marbles": ".*"/]]);
  });

  it("has Welcome routing", () => {
    assert.fileContent([
      [
        "src/app/app-routing.module.ts",
        "import { WelcomeComponent } from './welcome/welcome.component';",
      ],
    ]);
    assert.fileContent([
      [
        "src/app/app-routing.module.ts",
        "{ path: 'welcome', component: WelcomeComponent },",
      ],
    ]);
    assert.fileContent([
      [
        "src/app/app-routing.module.ts",
        "{ path: '', redirectTo: '/welcome', pathMatch: 'full' },",
      ],
    ]);
  });

  it("has PageNotFoundComponent routing", () => {
    assert.fileContent([
      [
        "src/app/app-routing.module.ts",
        "import { PageNotFoundComponent } from './error/page-not-found.component';",
      ],
    ]);
    assert.fileContent([
      [
        "src/app/app-routing.module.ts",
        "{ path: '**', component: PageNotFoundComponent },",
      ],
    ]);
  });

  describe("AppComponent", () => {
    it("has <app-header>", () => {
      assert.fileContent([
        [
          "src/app/app.component.html",
          `<app-header (toggleSideNavigation)="sidemenu.toggle()"></app-header>`,
        ],
      ]);
    });

    it("has <mat-sidenav-container>", () => {
      assert.fileContent([
        [
          "src/app/app.component.html",
          `<mat-sidenav-container class="sidenav-container">`,
        ],
      ]);
    });

    it("has <router-outlet>", () => {
      assert.fileContent([
        ["src/app/app.component.html", `<router-outlet></router-outlet>`],
      ]);
    });

    it("replaces content", () => {
      assert.noFileContent([
        ["src/app/app.component.html", `sample app is running!`],
      ]);
    });
  });
});
