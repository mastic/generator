"use strict";

const assert = require("yeoman-assert");
const { runStackTest } = require("./testRunner");

describe("angular-layout-with-test-app", () => {
  beforeAll(() =>
    runStackTest("test-app-angular-layout", "angular", ["angular-layout"])
  );

  it("angular stack creates files", () => {
    assert.file([
      ".browserslistrc",
      ".editorconfig",
      ".eslintrc.json",
      ".gitignore",
      "angular.json",
      "proxy.conf.json",
      "karma.conf.js",
      "package.json",
      "README.md",
      "src/app/app.component.html",
      "src/app/app.component.sass",
      "src/app/app.component.spec.ts",
      "src/app/app.component.ts",
      "src/app/app.module.ts",
      "src/app/app-routing.module.ts",
      "src/assets/.gitkeep",
      "src/environments/environment.prod.ts",
      "src/environments/environment.ts",
      "src/favicon.ico",
      "src/index.html",
      "src/main.ts",
      "src/polyfills.ts",
      "src/styles.sass",
      "src/test.ts",
      "tsconfig.app.json",
      "tsconfig.json",
      "tsconfig.spec.json",
    ]);
    assert.fileContent([
      ["README.md", `# Sample`],
      ["src/index.html", `<title>Sample</title>`],
    ]);
  });

  it("angular-layout aspect creates files", () => {
    assert.file([
      "src/app/elevate.directive.spec.ts",
      "src/app/elevate.directive.ts",
      "src/app/error/page-not-found.component.html",
      "src/app/error/page-not-found.component.sass",
      "src/app/error/page-not-found.component.spec.ts",
      "src/app/error/page-not-found.component.ts",
      "src/app/header/header.component.html",
      "src/app/header/header.component.sass",
      "src/app/header/header.component.spec.ts",
      "src/app/header/header.component.ts",
      "src/app/side-navigation/side-navigation.component.html",
      "src/app/side-navigation/side-navigation.component.sass",
      "src/app/side-navigation/side-navigation.component.spec.ts",
      "src/app/side-navigation/side-navigation.component.ts",
      "src/app/welcome/welcome.component.html",
      "src/app/welcome/welcome.component.sass",
      "src/app/welcome/welcome.component.spec.ts",
      "src/app/welcome/welcome.component.ts",
    ]);
  });

  describe("SharedModule", () => {
    it("imports other modules", () => {
      assert.fileContent([
        [
          "src/app/shared/shared.module.ts",
          "import { FlexLayoutModule } from '@angular/flex-layout';",
        ],
        [
          "src/app/shared/shared.module.ts",
          "import { MatToolbarModule } from '@angular/material/toolbar';",
        ],
        [
          "src/app/shared/shared.module.ts",
          "import { MatIconModule } from '@angular/material/icon';",
        ],
        [
          "src/app/shared/shared.module.ts",
          "import { MatSidenavModule } from '@angular/material/sidenav';",
        ],
      ]);
    });
  });

  describe("AppModule", () => {
    it("imports other modules", () => {
      assert.fileContent([
        [
          "src/app/app.module.ts",
          "import { WelcomeComponent } from './welcome/welcome.component';",
        ],
        [
          "src/app/app.module.ts",
          "import { HeaderComponent } from './header/header.component';",
        ],
        [
          "src/app/app.module.ts",
          "import { SideNavigationComponent } from './side-navigation/side-navigation.component';",
        ],
        [
          "src/app/app.module.ts",
          "import { PageNotFoundComponent } from './error/page-not-found.component';",
        ],
        [
          "src/app/app.module.ts",
          "import { ElevateDirective } from './elevate.directive';",
        ],
      ]);
    });
  });

  it("has Welcome routing", () => {
    assert.fileContent([
      [
        "src/app/app-routing.module.ts",
        "import { WelcomeComponent } from './welcome/welcome.component';",
      ],
      [
        "src/app/app-routing.module.ts",
        "{ path: 'welcome', component: WelcomeComponent },",
      ],
      [
        "src/app/app-routing.module.ts",
        "{ path: '', redirectTo: '/welcome', pathMatch: 'full' },",
      ],
    ]);
  });

  it("has PageNotFoundComponent routing", () => {
    assert.fileContent([
      [
        "src/app/app-routing.module.ts",
        "import { PageNotFoundComponent } from './error/page-not-found.component';",
      ],
      [
        "src/app/app-routing.module.ts",
        "{ path: '**', component: PageNotFoundComponent },",
      ],
    ]);
  });

  describe("AppComponent", () => {
    it("has <app-header>", () => {
      assert.fileContent([
        [
          "src/app/app.component.html",
          `<app-header (toggleSideNavigation)="sidemenu.toggle()"></app-header>`,
        ],
      ]);
    });

    it("has <mat-sidenav-container>", () => {
      assert.fileContent([
        [
          "src/app/app.component.html",
          `<mat-sidenav-container class="sidenav-container">`,
        ],
      ]);
    });

    it("has <router-outlet>", () => {
      assert.fileContent([
        ["src/app/app.component.html", `<router-outlet></router-outlet>`],
      ]);
    });

    it("replaces content", () => {
      assert.noFileContent([
        ["src/app/app.component.html", `sample app is running!`],
      ]);
    });
  });
});
