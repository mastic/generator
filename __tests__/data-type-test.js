"use strict";
const path = require("path");
const assert = require("yeoman-assert");
const fs = require("fs-extra");
const { readModel } = require("../generators/model-utils");
const { FieldVisitor } = require("../generators/data-type");

class SomeVisitor extends FieldVisitor {}

function someValue(field) {
  return field.accept(new SomeVisitor());
}

describe("Field Visitor default implementation throws error for", () => {
  let enumField;
  let stringField;
  let booleanField;
  let longField;
  let integerField;
  let doubleField;
  let datetimeField;
  let relationField;

  beforeAll(() => {
    const file = path.join(__dirname, "./model-test.yml");
    const model = readModel(fs, file);
    const entity = model.entities.filter(
      (entity) => entity.name === "TestEntity"
    )[0];
    enumField = entity.fields.filter((field) => field.name === "enumField")[0];
    stringField = entity.fields.filter(
      (field) => field.name === "stringField"
    )[0];
    booleanField = entity.fields.filter(
      (field) => field.name === "booleanField"
    )[0];
    longField = entity.fields.filter((field) => field.name === "longField")[0];
    integerField = entity.fields.filter(
      (field) => field.name === "integerField"
    )[0];
    doubleField = entity.fields.filter(
      (field) => field.name === "doubleField"
    )[0];
    datetimeField = entity.fields.filter(
      (field) => field.name === "datetimeField"
    )[0];
    relationField = entity.fields.filter(
      (field) => field.name === "relationField"
    )[0];
  });

  it("acceptEnum()", () => {
    assert.throws(() => someValue(enumField), Error);
  });

  it("acceptString()", () => {
    assert.throws(() => someValue(stringField), Error);
  });

  it("acceptBoolean()", () => {
    assert.throws(() => someValue(booleanField), Error);
  });

  it("acceptLong()", () => {
    assert.throws(() => someValue(longField), Error);
  });

  it("acceptInteger()", () => {
    assert.throws(() => someValue(integerField), Error);
  });

  it("acceptDouble()", () => {
    assert.throws(() => someValue(doubleField), Error);
  });

  it("acceptDatetime()", () => {
    assert.throws(() => someValue(datetimeField), Error);
  });

  it("acceptRelation()", () => {
    assert.throws(() => someValue(relationField), Error);
  });

  it("undefined field", () => {
    assert.throws(() => someValue(undefined), Error);
  });

  it("field with no type", () => {
    const noTypeField = doubleField;
    noTypeField.type = undefined;
    assert.throws(() => someValue(noTypeField), Error);
  });

  it("field with unknown type", () => {
    const unknownTypeField = doubleField;
    unknownTypeField.type = "dummy";
    assert.throws(() => someValue(unknownTypeField), Error);
  });
});
