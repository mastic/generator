"use strict";
const path = require("path");
const assert = require("yeoman-assert");
const helpers = require("yeoman-test");

describe("generator-mastic:spring-docker-vm", () => {
  beforeAll(() =>
    helpers.run(path.join(__dirname, "../generators/spring-docker-vm"))
  );

  it("creates files", () => {
    assert.file(["src/main/docker/Dockerfile.jvm"]);
  });
});
