"use strict";
const path = require("path");
const assert = require("yeoman-assert");
const helpers = require("yeoman-test");

describe("generator-mastic:angular-gitlab-ci", () => {
  beforeAll(() =>
    helpers.run(path.join(__dirname, "../generators/angular-gitlab-ci"))
  );

  it("creates files", () => {
    assert.file([".gitlab-ci.yml"]);
  });
});
