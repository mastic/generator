"use strict";
const path = require("path");
const assert = require("yeoman-assert");
const helpers = require("yeoman-test");

describe("generator-mastic:quarkus-example-resource", () => {
  beforeAll(() =>
    helpers.run(path.join(__dirname, "../generators/quarkus-example-resource"))
  );

  it("creates files", () => {
    assert.file([
      "src/main/resources/META-INF/resources/index.html",
      "src/native-test/java/com/mycompany/NativeExampleResourceIT.java",
      "src/main/java/com/mycompany/ExampleResource.java",
      "src/test/java/com/mycompany/ExampleResourceTest.java",
    ]);
  });
});
