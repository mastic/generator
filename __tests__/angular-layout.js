"use strict";
const path = require("path");
const assert = require("yeoman-assert");
const helpers = require("yeoman-test");

describe("generator-mastic:angular-layout", () => {
  beforeAll(() =>
    helpers.run(path.join(__dirname, "../generators/angular-layout"))
  );
  it("creates files", () => {
    assert.file([
      "src/app/elevate.directive.spec.ts",
      "src/app/elevate.directive.ts",
      "src/app/error/page-not-found.component.html",
      "src/app/error/page-not-found.component.sass",
      "src/app/error/page-not-found.component.spec.ts",
      "src/app/error/page-not-found.component.ts",
      "src/app/header/header.component.html",
      "src/app/header/header.component.sass",
      "src/app/header/header.component.spec.ts",
      "src/app/header/header.component.ts",
      "src/app/side-navigation/side-navigation.component.html",
      "src/app/side-navigation/side-navigation.component.sass",
      "src/app/side-navigation/side-navigation.component.spec.ts",
      "src/app/side-navigation/side-navigation.component.ts",
      "src/app/welcome/welcome.component.html",
      "src/app/welcome/welcome.component.sass",
      "src/app/welcome/welcome.component.spec.ts",
      "src/app/welcome/welcome.component.ts",
    ]);
  });

  it("has @angular/flex-layout dependency", () => {
    assert.fileContent([["package.json", /"@angular\/flex-layout": ".*"/]]);
  });

  it("has @angular/cdk dependency", () => {
    assert.fileContent([["package.json", /"@angular\/cdk": ".*"/]]);
  });
});
