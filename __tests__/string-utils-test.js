"use strict";
const str = require("../generators/string-utils");
const assert = require("yeoman-assert");

describe("string-utils", () => {
  it("invalid input for camelCapitalize() returns invalid result", () => {
    assert.strictEqual(str.camelCapitalize(""), "");
    assert.strictEqual(str.camelCapitalize(null), null);
    assert.strictEqual(str.camelCapitalize(undefined), undefined);
  });

  it("invalid input for camelUncapitalize() returns invalid result", () => {
    assert.strictEqual(str.camelUncapitalize(""), "");
    assert.strictEqual(str.camelUncapitalize(null), null);
    assert.strictEqual(str.camelUncapitalize(undefined), undefined);
  });

  it("invalid input for endpoint() returns invalid result", () => {
    assert.strictEqual(str.endpoint(""), "");
    assert.strictEqual(str.endpoint(null), null);
    assert.strictEqual(str.endpoint(undefined), undefined);
  });

  it("invalid input for pluralEndpoint() returns invalid result", () => {
    assert.strictEqual(str.pluralEndpoint(""), "");
    assert.strictEqual(str.pluralEndpoint(null), null);
    assert.strictEqual(str.pluralEndpoint(undefined), undefined);
  });

  it("invalid input for snakeCapitalize() returns invalid result", () => {
    assert.strictEqual(str.snakeCapitalize(""), "");
    assert.strictEqual(str.snakeCapitalize(null), null);
    assert.strictEqual(str.snakeCapitalize(undefined), undefined);
  });

  it("invalid input for snakeUncapitalize() returns invalid result", () => {
    assert.strictEqual(str.snakeUncapitalize(""), "");
    assert.strictEqual(str.snakeUncapitalize(null), null);
    assert.strictEqual(str.snakeUncapitalize(undefined), undefined);
  });

  it("invalid input for title() returns invalid result", () => {
    assert.strictEqual(str.title(""), "");
    assert.strictEqual(str.title(null), null);
    assert.strictEqual(str.title(undefined), undefined);
  });

  it("invalid input for pluralTitle() returns invalid result", () => {
    assert.strictEqual(str.pluralTitle(""), "");
    assert.strictEqual(str.pluralTitle(null), null);
    assert.strictEqual(str.pluralTitle(undefined), undefined);
  });
});
