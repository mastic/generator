"use strict";

const assert = require("yeoman-assert");
const { runStackTest } = require("./testRunner");

describe("angular-with-test-app", () => {
  beforeAll(() =>
    runStackTest("test-app-angular", "angular", [
      "angular-gitlab-ci",
      "angular-vscode",
    ])
  );

  it("angular stack creates files", () => {
    assert.file([
      ".browserslistrc",
      ".editorconfig",
      ".eslintrc.json",
      ".gitignore",
      "angular.json",
      "proxy.conf.json",
      "karma.conf.js",
      "package.json",
      "README.md",
      "src/app/app.component.html",
      "src/app/app.component.sass",
      "src/app/app.component.spec.ts",
      "src/app/app.component.ts",
      "src/app/app.module.ts",
      "src/app/app-routing.module.ts",
      "src/assets/.gitkeep",
      "src/environments/environment.prod.ts",
      "src/environments/environment.ts",
      "src/favicon.ico",
      "src/index.html",
      "src/main.ts",
      "src/polyfills.ts",
      "src/styles.sass",
      "src/test.ts",
      "tsconfig.app.json",
      "tsconfig.json",
      "tsconfig.spec.json",
    ]);
    assert.fileContent([
      ["README.md", `# Sample`],
      ["src/index.html", `<title>Sample</title>`],
    ]);
  });

  it("angular-gitlab-ci aspect creates files", () => {
    assert.file([".gitlab-ci.yml"]);
  });

  it("angular-vscode aspect creates files", () => {
    assert.file([".vscode/settings.json"]);
  });
});
