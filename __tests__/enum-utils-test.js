"use strict";
const path = require("path");
const assert = require("yeoman-assert");
const fs = require("fs-extra");
const { readModel } = require("../generators/model-utils");
const { properties } = require("../generators/enum-utils");

describe("enum-utils", () => {
  const dir = path.join(__dirname, "./model-test.yml");
  const model = readModel(fs, dir);

  it(".properties() with empty properties", () => {
    const enm = model.entities[0];

    assert.ok(properties(enm, undefined));
    assert.ok(properties(enm, {}));

    assert.strictEqual(
      JSON.stringify(properties(enm, undefined)),
      JSON.stringify(properties(enm, {}))
    );
  });
});
