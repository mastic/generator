"use strict";
const path = require("path");
const assert = require("yeoman-assert");
const helpers = require("yeoman-test");

describe("generator-mastic:angular-renovate", () => {
  beforeAll(() =>
    helpers.run(path.join(__dirname, "../generators/angular-renovate"))
  );

  it("creates files", () => {
    assert.file(["renovate.json"]);
  });
});
