"use strict";

const assert = require("yeoman-assert");

const { runStackTest } = require("./testRunner");

describe("angular-entity-with-test-app", () => {
  beforeAll(() =>
    runStackTest("test-app-angular-entity", "angular", ["angular-entity"])
  );

  it("angular stack creates files", () => {
    assert.file([
      ".browserslistrc",
      ".editorconfig",
      ".eslintrc.json",
      ".gitignore",
      "angular.json",
      "proxy.conf.json",
      "karma.conf.js",
      "package.json",
      "README.md",
      "src/app/app.component.html",
      "src/app/app.component.sass",
      "src/app/app.component.spec.ts",
      "src/app/app.component.ts",
      "src/app/app.module.ts",
      "src/app/app-routing.module.ts",
      "src/assets/.gitkeep",
      "src/environments/environment.prod.ts",
      "src/environments/environment.ts",
      "src/favicon.ico",
      "src/index.html",
      "src/main.ts",
      "src/polyfills.ts",
      "src/styles.sass",
      "src/test.ts",
      "tsconfig.app.json",
      "tsconfig.json",
      "tsconfig.spec.json",
    ]);
    assert.fileContent([
      ["README.md", `# Sample`],
      ["src/index.html", `<title>Sample</title>`],
    ]);
  });

  it("angular-entity aspect does not create files", () => {
    assert.noFile([
      "src/app/elevate.directive.spec.ts",
      "src/app/elevate.directive.ts",
      "src/app/error/page-not-found.component.html",
      "src/app/error/page-not-found.component.sass",
      "src/app/error/page-not-found.component.spec.ts",
      "src/app/error/page-not-found.component.ts",
      "src/app/header/header.component.html",
      "src/app/header/header.component.sass",
      "src/app/header/header.component.spec.ts",
      "src/app/header/header.component.ts",
      "src/app/side-navigation/side-navigation.component.html",
      "src/app/side-navigation/side-navigation.component.sass",
      "src/app/side-navigation/side-navigation.component.spec.ts",
      "src/app/side-navigation/side-navigation.component.ts",
      "src/app/welcome/welcome.component.html",
      "src/app/welcome/welcome.component.sass",
      "src/app/welcome/welcome.component.spec.ts",
      "src/app/welcome/welcome.component.ts",
    ]);
  });

  it("angular-entity creates files", () => {
    assert.file([
      "src/app/fruit/fruit.actions.ts",
      "src/app/fruit/fruit.component.html",
      "src/app/fruit/fruit.component.sass",
      "src/app/fruit/fruit.component.spec.ts",
      "src/app/fruit/fruit.component.ts",
      "src/app/fruit/fruit-delete-dialog.component.html",
      "src/app/fruit/fruit-delete-dialog.component.sass",
      "src/app/fruit/fruit-delete-dialog.component.spec.ts",
      "src/app/fruit/fruit-delete-dialog.component.ts",
      "src/app/fruit/fruit.effects.spec.ts",
      "src/app/fruit/fruit.effects.ts",
      "src/app/fruit/fruit-form.component.html",
      "src/app/fruit/fruit-form.component.sass",
      "src/app/fruit/fruit-form.component.spec.ts",
      "src/app/fruit/fruit-form.component.ts",
      "src/app/fruit/fruit.model.ts",
      "src/app/fruit/fruit.module.ts",
      "src/app/fruit/fruit.reducer.spec.ts",
      "src/app/fruit/fruit.reducer.ts",
      "src/app/fruit/fruit.selectors.spec.ts",
      "src/app/fruit/fruit.selectors.ts",
      "src/app/fruit/fruit.service.spec.ts",
      "src/app/fruit/fruit.service.ts",
      "src/app/fruit/fruit-update.component.html",
      "src/app/fruit/fruit-update.component.sass",
      "src/app/fruit/fruit-update.component.spec.ts",
      "src/app/fruit/fruit-update.component.ts",
      "src/app/some-entity/some-entity.actions.ts",
      "src/app/some-entity/some-entity.component.html",
      "src/app/some-entity/some-entity.component.sass",
      "src/app/some-entity/some-entity.component.spec.ts",
      "src/app/some-entity/some-entity.component.ts",
      "src/app/some-entity/some-entity-delete-dialog.component.html",
      "src/app/some-entity/some-entity-delete-dialog.component.sass",
      "src/app/some-entity/some-entity-delete-dialog.component.spec.ts",
      "src/app/some-entity/some-entity-delete-dialog.component.ts",
      "src/app/some-entity/some-entity.effects.spec.ts",
      "src/app/some-entity/some-entity.effects.ts",
      "src/app/some-entity/some-entity-form.component.html",
      "src/app/some-entity/some-entity-form.component.sass",
      "src/app/some-entity/some-entity-form.component.spec.ts",
      "src/app/some-entity/some-entity-form.component.ts",
      "src/app/some-entity/some-entity.model.ts",
      "src/app/some-entity/some-entity.module.ts",
      "src/app/some-entity/some-entity.reducer.spec.ts",
      "src/app/some-entity/some-entity.reducer.ts",
      "src/app/some-entity/some-entity.selectors.spec.ts",
      "src/app/some-entity/some-entity.selectors.ts",
      "src/app/some-entity/some-entity.service.spec.ts",
      "src/app/some-entity/some-entity.service.ts",
      "src/app/some-entity/some-entity-update.component.html",
      "src/app/some-entity/some-entity-update.component.sass",
      "src/app/some-entity/some-entity-update.component.spec.ts",
      "src/app/some-entity/some-entity-update.component.ts",
      "src/app/some-type/some-type.ts",
    ]);
  });

  it("angular-entity aspect creates files", () => {
    assert.file([
      "src/app/reducers/common.ts",
      "src/app/reducers/debug.ts",
      "src/app/reducers/index.ts",
    ]);
  });

  describe("package.json", () => {
    it("has dependencies", () => {
      assert.fileContent([
        ["package.json", /"@ngrx\/effects": ".*"/],
        ["package.json", /"@ngrx\/entity": ".*"/],
        ["package.json", /"@ngrx\/store": ".*"/],
        ["package.json", /"@ngrx\/store-devtools": ".*"/],
        ["package.json", /"@ngrx\/schematics": ".*"/],
        ["package.json", /"@types\/uuid": ".*"/],
        ["package.json", /"jasmine-marbles": ".*"/],
      ]);
    });

    describe("src/app/reducers/index.ts", () => {
      it("has imports", () => {
        assert.fileContent([
          [
            "src/app/reducers/index.ts",
            `import * as fromFruit from '../fruit/fruit.reducer';`,
          ],
          [
            "src/app/reducers/index.ts",
            `import * as fromSomeEntity from '../some-entity/some-entity.reducer';`,
          ],
        ]);
      });
      it("has states", () => {
        assert.fileContent([
          [
            "src/app/reducers/index.ts",
            `[fromFruit.fruitsFeatureKey]: fromFruit.State;`,
          ],
          [
            "src/app/reducers/index.ts",
            `[fromSomeEntity.someEntitiesFeatureKey]: fromSomeEntity.State;`,
          ],
        ]);
      });
      it("has reducers", () => {
        assert.fileContent([
          [
            "src/app/reducers/index.ts",
            `[fromFruit.fruitsFeatureKey]: fromFruit.reducer,`,
          ],
          [
            "src/app/reducers/index.ts",
            `[fromSomeEntity.someEntitiesFeatureKey]: fromSomeEntity.reducer,`,
          ],
        ]);
      });
    });

    describe("AppRoutingModule", () => {
      it("has Fruit routing", () => {
        assert.fileContent([
          [
            "src/app/app-routing.module.ts",
            "import { FruitComponent } from './fruit/fruit.component';",
          ],
          [
            "src/app/app-routing.module.ts",
            "{ path: 'fruit', component: FruitComponent },",
          ],
        ]);
      });

      it("has Fruit update routing", () => {
        assert.fileContent([
          [
            "src/app/app-routing.module.ts",
            "import { FruitUpdateComponent } from './fruit/fruit-update.component';",
          ],
          [
            "src/app/app-routing.module.ts",
            "{ path: 'fruit/:id', component: FruitUpdateComponent },",
          ],
        ]);
      });

      it("has SomeEntity routing", () => {
        assert.fileContent([
          [
            "src/app/app-routing.module.ts",
            "import { SomeEntityComponent } from './some-entity/some-entity.component';",
          ],
          [
            "src/app/app-routing.module.ts",
            "{ path: 'some-entity', component: SomeEntityComponent },",
          ],
        ]);
      });

      it("has SomeEntity update routing", () => {
        assert.fileContent([
          [
            "src/app/app-routing.module.ts",
            "import { SomeEntityUpdateComponent } from './some-entity/some-entity-update.component';",
          ],
          [
            "src/app/app-routing.module.ts",
            "{ path: 'some-entity/:id', component: SomeEntityUpdateComponent },",
          ],
        ]);
      });
    });

    describe("SharedModule", () => {
      it("imports other modules", () => {
        assert.fileContent([
          [
            "src/app/shared/shared.module.ts",
            "import { HttpClientModule } from '@angular/common/http';",
          ],
          [
            "src/app/shared/shared.module.ts",
            "import { MatButtonModule } from '@angular/material/button';",
          ],
          [
            "src/app/shared/shared.module.ts",
            "import { MatDividerModule } from '@angular/material/divider';",
          ],
          [
            "src/app/shared/shared.module.ts",
            "import { MatListModule } from '@angular/material/list';",
          ],
          [
            "src/app/shared/shared.module.ts",
            "import { MatFormFieldModule } from '@angular/material/form-field';",
          ],
          [
            "src/app/shared/shared.module.ts",
            "import { MatInputModule } from '@angular/material/input';",
          ],
          [
            "src/app/shared/shared.module.ts",
            "import { MatTableModule } from '@angular/material/table';",
          ],
          [
            "src/app/shared/shared.module.ts",
            "import { MatExpansionModule } from '@angular/material/expansion';",
          ],
          [
            "src/app/shared/shared.module.ts",
            "import { MatCardModule } from '@angular/material/card';",
          ],
          [
            "src/app/shared/shared.module.ts",
            "import { MatCheckboxModule } from '@angular/material/checkbox';",
          ],
          [
            "src/app/shared/shared.module.ts",
            "import { MatDialogModule } from '@angular/material/dialog';",
          ],
        ]);
      });
    });

    describe("AppModule", () => {
      it("imports other modules", () => {
        assert.fileContent([
          [
            "src/app/app.module.ts",
            "import { StoreModule } from '@ngrx/store';",
          ],
          [
            "src/app/app.module.ts",
            "import { EffectsModule } from '@ngrx/effects';",
          ],
          [
            "src/app/app.module.ts",
            "import { StoreDevtoolsModule } from '@ngrx/store-devtools';",
          ],
          [
            "src/app/app.module.ts",
            "import { reducers, metaReducers } from './reducers';",
          ],
          [
            "src/app/app.module.ts",
            "import { environment } from '../environments/environment';",
          ],
          [
            "src/app/app.module.ts",
            "import { FruitEffects } from './fruit/fruit.effects';",
          ],
          [
            "src/app/app.module.ts",
            "import { FruitModule } from './fruit/fruit.module';",
          ],
          ["src/app/app.module.ts", "FruitModule,"],
          ["src/app/app.module.ts", "EffectsModule.forRoot(["],
          [
            "src/app/app.module.ts",
            "StoreModule.forRoot(reducers, { metaReducers }),",
          ],
          [
            "src/app/app.module.ts",
            "StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production }),",
          ],
        ]);
      });
    });
  });
});
