"use strict";
const path = require("path");
const assert = require("yeoman-assert");
const helpers = require("yeoman-test");

describe("generator-mastic:angular-vscode", () => {
  beforeAll(() =>
    helpers.run(path.join(__dirname, "../generators/angular-vscode"))
  );

  it("creates files", () => {
    assert.file([".vscode/settings.json"]);
  });
});
