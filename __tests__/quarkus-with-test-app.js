"use strict";

const assert = require("yeoman-assert");
const { runStackTest } = require("./testRunner");

describe("quarkus-with-test-app", () => {
  beforeAll(() =>
    runStackTest("test-app-quarkus-liquibase", "quarkus", [
      "quarkus-reactive-postgresql",
      "quarkus-liquibase",
    ])
  );

  it("quarkus stack creates files", () => {
    assert.file([
      "docker-compose.yml",
      ".dockerignore",
      ".gitignore",
      "README.md",
      "build.gradle",
      "gradle.properties",
      "settings.gradle",
      "build.gradle",
      "gradlew.bat",
      "gradlew",
      "gradle/wrapper/gradle-wrapper.properties",
      "gradle/wrapper/gradle-wrapper.jar",
      "src/main/resources/application.properties",
      "src/main/java/com/example/SomeType.java",
    ]);
  });

  it("quarkus-reactive-postgresql aspect creates files", () => {
    assert.file([
      "src/main/java/com/example/ReactiveCrudRepository.java",
      "src/main/java/com/example/Fruit.java",
      "src/main/java/com/example/FruitRepository.java",
      "src/main/java/com/example/FruitResource.java",
      "src/main/java/com/example/SomeEntity.java",
      "src/main/java/com/example/SomeEntityRepository.java",
      "src/main/java/com/example/SomeEntityResource.java",
      "src/test/java/com/example/SomeEntityResourceTest.java",
      "src/test/java/com/example/FruitResourceTest.java",
      "src/native-test/java/com/example/NativeSomeEntityResourceTest.java",
      "src/native-test/java/com/example/NativeFruitResourceTest.java",
    ]);

    assert.fileContent("docker-compose.yml", "- POSTGRES_DB=sample");
    assert.fileContent([
      [
        "build.gradle",
        `implementation 'io.quarkus:quarkus-reactive-pg-client'`,
      ],
      ["build.gradle", `implementation 'io.quarkus:quarkus-vertx'`],
      ["build.gradle", `implementation 'io.quarkus:quarkus-resteasy'`],
      ["build.gradle", `implementation 'io.quarkus:quarkus-resteasy-jackson'`],
      ["build.gradle", `implementation 'io.quarkus:quarkus-resteasy-mutiny'`],
      [
        "build.gradle",
        `implementation 'io.netty:netty-transport-native-epoll'`,
      ],
      ["build.gradle", `implementation 'org.apache.commons:commons-lang3'`],
    ]);

    assert.fileContent([
      [
        "src/main/resources/application.properties",
        `quarkus.vertx.prefer-native-transport=true`,
      ],
      [
        "src/main/resources/application.properties",
        `quarkus.http.so-reuse-port=true`,
      ],
      [
        "src/main/resources/application.properties",
        `quarkus.datasource.db-kind=postgresql`,
      ],
      [
        "src/main/resources/application.properties",
        `quarkus.datasource.username=dbuser`,
      ],
      [
        "src/main/resources/application.properties",
        `quarkus.datasource.password=dbpassword`,
      ],
      [
        "src/main/resources/application.properties",
        `quarkus.datasource.reactive.url=postgresql://postgresdb:5432/sample`,
      ],
    ]);
  });

  it("quarkus-liquibase aspect creates files", () => {
    assert.file([
      "src/main/resources/db/changeLog.xml",
      "src/main/resources/db/initial_schema.xml",
      "src/main/resources/db/entity_Fruit.xml",
      "src/main/resources/db/entity_SomeEntity.xml",
    ]);
    assert.fileContent([
      [
        "src/main/resources/db/changeLog.xml",
        `<include file="db/entity_Fruit.xml"`,
      ],
      [
        "src/main/resources/db/changeLog.xml",
        `<include file="db/entity_SomeEntity.xml"`,
      ],
      [
        "src/main/resources/application.properties",
        `quarkus.datasource.jdbc.url=jdbc:postgresql://postgresdb:5432/sample`,
      ],
      [
        "src/main/resources/application.properties",
        `quarkus.liquibase.migrate-at-start=true`,
      ],
    ]);
  });
});
