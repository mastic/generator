"use strict";
const path = require("path");
const assert = require("yeoman-assert");
const fs = require("fs-extra");
const { readModel } = require("../generators/model-utils");

describe("model-utils invalid relations", () => {
  it("throws error while reading the model", () => {
    const dir = path.join(__dirname, "./model-relations-invalid-test.yml");
    assert.throws(() => readModel(fs, dir), Error);
  });
});
