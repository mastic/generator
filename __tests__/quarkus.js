"use strict";
const path = require("path");
const assert = require("yeoman-assert");
const helpers = require("yeoman-test");

describe("generator-mastic:quarkus", () => {
  beforeAll(() => helpers.run(path.join(__dirname, "../generators/quarkus")));

  it("creates files", () => {
    assert.file([
      ".dockerignore",
      ".gitignore",
      "README.md",
      "build.gradle",
      "gradle.properties",
      "settings.gradle",
      "build.gradle",
      "gradlew.bat",
      "gradlew",
      "gradle/wrapper/gradle-wrapper.properties",
      "gradle/wrapper/gradle-wrapper.jar",
      "src/main/resources/application.properties",
    ]);
  });
});
