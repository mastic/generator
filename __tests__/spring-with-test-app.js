"use strict";

const assert = require("yeoman-assert");
const { runStackTest } = require("./testRunner");

describe("spring-with-test-app", () => {
  beforeAll(() =>
    runStackTest("test-app-spring", "spring", [
      "spring-docker-vm",
      "spring-docker-native",
      "spring-gitlab-ci",
      "spring-reactive-postgresql",
      "spring-umlmodel",
    ])
  );

  it("spring stack creates files", () => {
    assert.file([
      ".dockerignore",
      ".gitignore",
      "README.md",
      "build.gradle",
      "settings.gradle",
      "build.gradle",
      "gradlew.bat",
      "gradlew",
      "gradle/wrapper/gradle-wrapper.properties",
      "gradle/wrapper/gradle-wrapper.jar",
      "src/main/resources/application.yml",
      "src/test/resources/application-test.yml",
      "src/main/java/com/example/SomeType.java",
      "src/main/java/com/example/SampleApplication.java",
      "src/test/java/com/example/SampleApplicationTest.java",
    ]);
  });

  it("spring-docker-vm creates files", () => {
    assert.file(["src/main/docker/Dockerfile.jvm"]);
  });

  it("spring-docker-native creates files", () => {
    assert.file(["src/main/docker/Dockerfile.native"]);
  });

  it("spring-gitlab-ci creates files", () => {
    assert.file([".gitlab-ci.yml"]);
  });

  it("spring-reactive-postgresql creates files", () => {
    assert.file([
      "docker-compose.yml",
      "src/main/resources/db/migration/V0__init.sql",
      "src/test/resources/db/migration/V0__cleanup.sql",
      "src/main/java/com/example/db/DatabaseConfiguration.java",
      "src/main/java/com/example/db/DatabaseMigrationProperties.java",
      "src/main/java/com/example/exception/ExceptionHandler.java",
      "src/main/java/com/example/exception/NotFoundException.java",
      "src/main/java/com/example/util/FluxUtils.java",
      "src/main/java/com/example/Fruit.java",
      "src/main/java/com/example/FruitRepository.java",
      "src/main/java/com/example/FruitController.java",
      "src/main/java/com/example/SomeEntity.java",
      "src/main/java/com/example/SomeEntityRepository.java",
      "src/main/java/com/example/SomeEntityController.java",
      "src/test/java/com/example/FruitControllerTest.java",
      "src/test/java/com/example/SomeEntityControllerTest.java",
    ]);
  });

  it("spring-umlmodel creates files", () => {
    assert.file(["model.puml"]);
  });

  it("spring-umlmodel updates README.md", () => {
    assert.fileContent([
      ["README.md", "## Model"],
      ["README.md", "```plantuml"],
    ]);
  });

  it("spring-reactive-postgresql injects DB init script", () => {
    assert.fileContent([
      ["src/main/resources/application.yml", "V0__init.sql"],
    ]);
  });

  it("spring-reactive-postgresql injects DB cleanup script", () => {
    assert.fileContent([
      ["src/test/resources/application-test.yml", "V0__cleanup.sql"],
    ]);
  });

  it("spring-reactive-postgresql injects DB r2dbc prod settings", () => {
    assert.fileContent([
      ["src/main/resources/application.yml", "url: 'r2dbc:postgresql:"],
    ]);
  });

  it("spring-reactive-postgresql injects DB r2dbc test settings", () => {
    assert.fileContent([
      ["src/test/resources/application-test.yml", "url: 'r2dbc:h2:"],
    ]);
  });
});
