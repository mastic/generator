"use strict";
const path = require("path");
const assert = require("yeoman-assert");
const fs = require("fs-extra");
const { readModel } = require("../generators/model-utils");
const {
  OneToOne,
  OneToMany,
  ManyToOne,
  ManyToMany,
} = require("../generators/relations");

describe("model-utils relations", () => {
  let entity1;
  let entity2;
  let entity3;
  let entity4;

  beforeAll(() => {
    const dir = path.join(__dirname, "./model-relations-test.yml");
    const model = readModel(fs, dir);
    entity1 = model.entities.filter((e) => e.name === "Entity1")[0];
    entity2 = model.entities.filter((e) => e.name === "Entity2")[0];
    entity3 = model.entities.filter((e) => e.name === "Entity3")[0];
    entity4 = model.entities.filter((e) => e.name === "Entity4")[0];
  });

  it("reads entities", () => {
    assert.ok(entity1);
    assert.ok(entity2);
    assert.ok(entity2);
    assert.ok(entity3);
  });

  it("reads one-to-one bidirectional relation", () => {
    const entity1ToEntity2 = entity1.fields.filter(
      (f) => f.name === "oneToOneBi"
    )[0];
    assert.ok(entity1ToEntity2);
    assert.ok(entity1ToEntity2.isRelation());
    assert.strictEqual(entity1ToEntity2.getRelation(), OneToOne);
    assert.strictEqual("Entity2", entity1ToEntity2.type, "Entity2");
    assert.strictEqual(entity1ToEntity2.relation, OneToOne);

    const entity2ToEntity1 = entity2.fields.filter(
      (f) => f.name === "oneToOneBi"
    )[0];
    assert.ok(entity2ToEntity1);
    assert.ok(entity2ToEntity1.isRelation());
    assert.strictEqual(entity2ToEntity1.getRelation(), OneToOne);
    assert.strictEqual(entity2ToEntity1.type, "Entity1");
    assert.strictEqual(entity2ToEntity1.relation, OneToOne);
  });

  it("reads one-to-many bidirectional relation", () => {
    const entity2ToEntity3 = entity2.fields.filter(
      (f) => f.name === "oneToManyBi"
    )[0];
    assert.ok(entity2ToEntity3);
    assert.ok(entity2ToEntity3.isRelation());
    assert.strictEqual(entity2ToEntity3.getRelation(), OneToMany);
    assert.strictEqual(entity2ToEntity3.type, "Entity3");
    assert.strictEqual(entity2ToEntity3.relation, OneToMany);
  });

  it("reads many-to-one bidirectional relation", () => {
    const entity3ToEntity2 = entity3.fields.filter(
      (f) => f.name === "manyToOneBi"
    )[0];
    assert.ok(entity3ToEntity2);
    assert.ok(entity3ToEntity2.isRelation());
    assert.strictEqual(entity3ToEntity2.getRelation(), ManyToOne);
    assert.strictEqual(entity3ToEntity2.type, "Entity2");
    assert.strictEqual(entity3ToEntity2.relation, ManyToOne);
  });

  it("reads many-to-many bidirectional relation", () => {
    const entity3ToEntity4 = entity3.fields.filter(
      (f) => f.name === "manyToManyBi"
    )[0];
    assert.ok(entity3ToEntity4);
    assert.ok(entity3ToEntity4.isRelation());
    assert.strictEqual(entity3ToEntity4.getRelation(), ManyToMany);
    assert.strictEqual(entity3ToEntity4.type, "Entity4");
    assert.strictEqual(entity3ToEntity4.relation, ManyToMany);

    const entity4ToEntity3 = entity4.fields.filter(
      (f) => f.name === "manyToManyBi"
    )[0];
    assert.ok(entity4ToEntity3);
    assert.ok(entity4ToEntity3.isRelation());
    assert.strictEqual(entity4ToEntity3.getRelation(), ManyToMany);
    assert.strictEqual(entity4ToEntity3.type, "Entity3");
    assert.strictEqual(entity4ToEntity3.relation, ManyToMany);
  });

  it("reads non-relation field", () => {
    const entity1Name = entity1.fields.filter((f) => f.name === "name")[0];
    const entity2Name = entity2.fields.filter((f) => f.name === "name")[0];
    const entity3Name = entity3.fields.filter((f) => f.name === "name")[0];
    const entity4Name = entity4.fields.filter((f) => f.name === "name")[0];

    assert.ok(entity1Name);
    assert.ok(entity2Name);
    assert.ok(entity3Name);
    assert.ok(entity4Name);
    assert.strictEqual(entity1Name.isRelation(), false);
    assert.strictEqual(entity2Name.isRelation(), false);
    assert.strictEqual(entity3Name.isRelation(), false);
    assert.strictEqual(entity4Name.isRelation(), false);
    assert.strictEqual(entity1Name.getRelation(), undefined);
    assert.strictEqual(entity2Name.getRelation(), undefined);
    assert.strictEqual(entity3Name.getRelation(), undefined);
    assert.strictEqual(entity4Name.getRelation(), undefined);
    assert.strictEqual(entity1Name.relation, undefined);
    assert.strictEqual(entity2Name.relation, undefined);
    assert.strictEqual(entity3Name.relation, undefined);
    assert.strictEqual(entity4Name.relation, undefined);
    assert.strictEqual(entity1Name.type, "String");
    assert.strictEqual(entity2Name.type, "String");
    assert.strictEqual(entity3Name.type, "String");
    assert.strictEqual(entity4Name.type, "String");
  });
});
