"use strict";
const path = require("path");
const os = require("os");
const fs = require("fs");
const assert = require("yeoman-assert");
const { injectYaml } = require("../generators/yaml-utils");
const { readYaml, writeYaml } = require("../generators/yaml-utils");

describe("yaml-utils", () => {
  const dir = fs.mkdtempSync(path.join(os.tmpdir(), "yaml"));

  it("error occurs while reading", () => {
    assert.strictEqual(undefined, readYaml(undefined, undefined));
    assert.strictEqual(undefined, readYaml(undefined, undefined));
    assert.strictEqual(undefined, readYaml(undefined, "dummy-file.yml"));
    assert.strictEqual(undefined, readYaml(fs, undefined));
  });

  it("error occurs while writing", () => {
    const log = (_str) => {};
    assert.strictEqual(undefined, writeYaml(undefined, undefined, {}, log));
    assert.strictEqual(undefined, writeYaml(undefined, "dummy.yml", {}, log));
    assert.strictEqual(undefined, writeYaml(fs, undefined, {}, undefined));
  });

  it("writes and reads empty document", () => {
    const file = path.join(dir, "./empty.yml");
    const content = {};
    writeYaml(fs, file, content);
    const result = readYaml(fs, file);
    assert.deepStrictEqual(content, result);
  });

  it("writes and reads simple document", () => {
    const file = path.join(dir, "./simple.yml");

    const content = {
      string: "value",
      num: 42,
    };
    writeYaml(fs, file, content);
    const result = readYaml(fs, file);
    assert.deepStrictEqual(content, result);
  });

  it("injects undefined content into root", () => {
    const file = path.join(dir, "./file.yml");
    const content = {
      string: "value",
      num: 42,
    };
    writeYaml(fs, file, content);
    injectYaml(fs, file, "new", undefined);

    const result = readYaml(fs, file);
    assert.deepStrictEqual(content, result);
  });

  it("injects empty yaml into root", () => {
    const file = path.join(dir, "./file.yml");
    const content = {
      string: "value",
      num: 42,
    };
    writeYaml(fs, file, content);
    injectYaml(fs, file, "new", {});

    const result = readYaml(fs, file);
    assert.deepStrictEqual({ ...content, new: {} }, result);
  });

  it("injects yaml into root", () => {
    const file = path.join(dir, "./file.yml");
    const content = {
      string: "value",
      num: 42,
    };
    writeYaml(fs, file, content);
    injectYaml(fs, file, "new", { hello: "world" });

    const result = readYaml(fs, file);
    assert.deepStrictEqual({ ...content, new: { hello: "world" } }, result);
  });

  it("injects yaml into root with empty path", () => {
    const file = path.join(dir, "./file.yml");
    const content = {
      string: "value",
      num: 42,
    };
    writeYaml(fs, file, content);
    injectYaml(fs, file, "", { hello: "world" });

    const result = readYaml(fs, file);
    assert.deepStrictEqual({ ...content, hello: "world" }, result);
  });

  it("injects yaml into sub", () => {
    const file = path.join(dir, "./file.yml");
    const content = {
      num: 42,
    };
    writeYaml(fs, file, content);
    injectYaml(fs, file, "new.value", { hello: "world" });

    const result = readYaml(fs, file);
    assert.deepStrictEqual(
      { ...content, new: { value: { hello: "world" } } },
      result
    );
  });

  it("merges yaml tree", () => {
    const file = path.join(dir, "./file.yml");
    const content = {
      string: "value",
      num: 42,
      some: { existing: { value: 0 } },
    };
    writeYaml(fs, file, content);
    injectYaml(fs, file, "some", { hello: "world" });

    const result = readYaml(fs, file);
    assert.deepStrictEqual(
      {
        string: "value",
        num: 42,
        some: {
          existing: { value: 0 },
          hello: "world",
        },
      },
      result
    );
  });
});
