"use strict";
const path = require("path");
const fs = require("fs-extra");
const assert = require("yeoman-assert");
const helpers = require("yeoman-test");

describe("generator-mastic:quarkus-reactive-postgresql", () => {
  beforeAll(() =>
    helpers
      .run(path.join(__dirname, "../generators/quarkus-reactive-postgresql"))
      .inTmpDir((dir) => {
        fs.copySync(path.join(__dirname, "test-app-quarkus-postgresql"), dir);
      })
  );

  it("creates files", () => {
    assert.file([
      "docker-compose.yml",
      "src/main/java/com/example/ReactiveCrudRepository.java",
      "src/main/java/com/example/Fruit.java",
      "src/main/java/com/example/FruitRepository.java",
      "src/main/java/com/example/FruitResource.java",
      "src/main/java/com/example/SomeEntity.java",
      "src/main/java/com/example/SomeEntityRepository.java",
      "src/main/java/com/example/SomeEntityResource.java",
      "src/test/java/com/example/FruitRepositoryTest.java",
      "src/test/java/com/example/FruitResourceTest.java",
      "src/test/java/com/example/SomeEntityRepositoryTest.java",
      "src/test/java/com/example/SomeEntityResourceTest.java",
      "src/native-test/java/com/example/NativeSomeEntityResourceTest.java",
      "src/native-test/java/com/example/NativeFruitResourceTest.java",
    ]);
  });
});
