"use strict";
const path = require("path");
const assert = require("yeoman-assert");
const fs = require("fs-extra");
const { readModel } = require("../generators/model-utils");
const {
  dbType,
  javaImports,
  javaType,
  testValue,
  testValueUpdated,
} = require("../generators/type-utils");

describe("type-utils", () => {
  let enumField;
  let stringField;
  let booleanField;
  let longField;
  let integerField;
  let doubleField;
  let datetimeField;
  let enumFieldOptional;
  let stringFieldOptional;
  let booleanFieldOptional;
  let longFieldOptional;
  let integerFieldOptional;
  let doubleFieldOptional;
  let datetimeFieldOptional;

  beforeAll(() => {
    const file = path.join(__dirname, "./model-test.yml");
    const model = readModel(fs, file);
    const entity = model.entities.filter(
      (entity) => entity.name === "TestEntity"
    )[0];
    enumField = entity.fields.filter((field) => field.name === "enumField")[0];
    stringField = entity.fields.filter(
      (field) => field.name === "stringField"
    )[0];
    booleanField = entity.fields.filter(
      (field) => field.name === "booleanField"
    )[0];
    longField = entity.fields.filter((field) => field.name === "longField")[0];
    integerField = entity.fields.filter(
      (field) => field.name === "integerField"
    )[0];
    doubleField = entity.fields.filter(
      (field) => field.name === "doubleField"
    )[0];
    datetimeField = entity.fields.filter(
      (field) => field.name === "datetimeField"
    )[0];
    enumFieldOptional = entity.fields.filter(
      (field) => field.name === "enumFieldOptional"
    )[0];
    stringFieldOptional = entity.fields.filter(
      (field) => field.name === "stringFieldOptional"
    )[0];
    booleanFieldOptional = entity.fields.filter(
      (field) => field.name === "booleanFieldOptional"
    )[0];
    longFieldOptional = entity.fields.filter(
      (field) => field.name === "longFieldOptional"
    )[0];
    integerFieldOptional = entity.fields.filter(
      (field) => field.name === "integerFieldOptional"
    )[0];
    doubleFieldOptional = entity.fields.filter(
      (field) => field.name === "doubleFieldOptional"
    )[0];
    datetimeFieldOptional = entity.fields.filter(
      (field) => field.name === "datetimeFieldOptional"
    )[0];
  });

  it(".dbType()", () => {
    assert.strictEqual(dbType(enumField), "VARCHAR");
    assert.strictEqual(dbType(stringField), "VARCHAR NOT NULL");
    assert.strictEqual(dbType(booleanField), "BOOLEAN");
    assert.strictEqual(dbType(longField), "INTEGER NOT NULL");
    assert.strictEqual(dbType(integerField), "INTEGER NOT NULL");
    assert.strictEqual(dbType(doubleField), "DOUBLE PRECISION NOT NULL");
    assert.strictEqual(dbType(datetimeField), "TIMESTAMP NOT NULL");
  });

  it(".dbType() for optional", () => {
    assert.strictEqual(dbType(enumFieldOptional), "VARCHAR");
    assert.strictEqual(dbType(stringFieldOptional), "VARCHAR");
    assert.strictEqual(dbType(booleanFieldOptional), "BOOLEAN");
    assert.strictEqual(dbType(longFieldOptional), "INTEGER");
    assert.strictEqual(dbType(integerFieldOptional), "INTEGER");
    assert.strictEqual(dbType(doubleFieldOptional), "DOUBLE PRECISION");
    assert.strictEqual(dbType(datetimeFieldOptional), "TIMESTAMP");
  });

  it(".javaType()", () => {
    assert.strictEqual(javaType(enumField), "SomeType");
    assert.strictEqual(javaType(stringField), "String");
    assert.strictEqual(javaType(booleanField), "Boolean");
    assert.strictEqual(javaType(longField), "Long");
    assert.strictEqual(javaType(integerField), "Integer");
    assert.strictEqual(javaType(doubleField), "Double");
    assert.strictEqual(javaType(datetimeField), "Instant");
  });

  it(".javaImports()", () => {
    assert.strictEqual(javaImports(enumField), "");
    assert.strictEqual(javaImports(stringField), "");
    assert.strictEqual(javaImports(booleanField), "");
    assert.strictEqual(javaImports(longField), "");
    assert.strictEqual(javaImports(integerField), "");
    assert.strictEqual(javaImports(doubleField), "");
    assert.strictEqual(javaImports(datetimeField), "import java.time.Instant;");
  });

  it(".testValue()", () => {
    assert.ok(testValue(enumField));
    assert.ok(testValue(stringField));
    assert.notStrictEqual(testValue(booleanField), undefined);
    assert.ok(testValue(longField));
    assert.ok(testValue(integerField));
    assert.ok(testValue(doubleField));
    assert.ok(testValue(datetimeField));
  });

  it(".testValueUpdated()", () => {
    assert.ok(testValueUpdated(enumField));
    assert.ok(testValueUpdated(stringField));
    assert.notStrictEqual(testValueUpdated(booleanField), undefined);
    assert.ok(testValueUpdated(longField));
    assert.ok(testValueUpdated(integerField));
    assert.ok(testValueUpdated(doubleField));
    assert.ok(testValueUpdated(datetimeField));
  });
});
