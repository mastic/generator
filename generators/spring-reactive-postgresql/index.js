"use strict";
const _ = require("lodash");
const BaseGenerator = require("../base-generator");
const str = require("../string-utils");
const pin = require("../pin-utils");
const entityUtils = require("../spring/entity-utils");
const props = entityUtils.properties;
const { columnName } = entityUtils;
const type = require("../type-utils");
const { injectYaml } = require("../yaml-utils");
const { OneToOne, ManyToOne } = require("../relations");

const cap = str.camelCapitalize;
const uncap = str.camelUncapitalize;
const tableName = str.snakeCapitalize;
const { dbType } = type;
const { addGradleDependency, addAnnotation } = pin;
const val = type.testValue;

module.exports = class extends BaseGenerator {
  properties() {
    this.applicationName = cap(this.application.app.name);
    if (this.model && this.model.entities) {
      this.application.DeleteTables = _.chain(this.model.entities)
        .map((e) => `DELETE FROM "T_${tableName(e.name)}";`)
        .join(`\n`)
        .value();
      this.application.TableNames = _.chain(this.model.entities)
        .map((e) => `T_${tableName(e.name)}`)
        .value();
      this.application.EntityNames = _.chain(this.model.entities)
        .map((e) => e.name)
        .value();
      this.application.CreateTableFields = {};
      this.application.CreateTableConstraints = {};
      this.application.entityName = {};
      this.application.TestDataSetups = {};
      this.model.entities.forEach((entity) => {
        const tblName = `T_${tableName(entity.name)}`;
        this.application.CreateTableFields[tblName] = _.chain(entity.fields)
          .filter(
            (f) =>
              !f.isRelation() ||
              f.getRelation() === ManyToOne ||
              f.getRelation() === OneToOne
          )
          .map((f) => `${columnName(f)} ${dbType(f)}`)
          .join(`,\n    `)
          .value();
        this.application.CreateTableConstraints[tblName] = _.chain(
          entity.fields
        )
          .filter(
            (f) => f.getRelation() === ManyToOne || f.getRelation() === OneToOne
          )
          .map(
            (f) =>
              `,\n    CONSTRAINT FK_${tableName(
                entity.name + "_" + f.name
              )} FOREIGN KEY (${columnName(f)}) REFERENCES T_${tableName(
                f.type
              )}(ID) ON DELETE CASCADE`
          )
          .join(``)
          .value();
        this.application.entityName[entity.name] = uncap(entity.name);
        this.application.TestDataSetups[entity.name] = _.chain(entity.fields)
          .filter(
            (f) =>
              !f.isRelation() ||
              f.getRelation() === ManyToOne ||
              f.getRelation() === OneToOne
          )
          .map((f) => {
            let setter = `set${cap(f.name)}`;
            if (f.isRelation()) setter = `set${cap(f.name)}Id`;
            return `${uncap(entity.name)}.${setter}(${val(f)});`;
          })
          .join(`\n        `)
          .value();
      });
      this.application.TestDataEntityFields = _.chain(this.model.entities)
        .map((e) => `private ${cap(e.name)} ${uncap(e.name)};`)
        .join(`\n  `)
        .value();
      this.application.TestDataGetters = _.chain(this.model.entities)
        .map(
          (e) =>
            `public ${cap(e.name)} get${cap(e.name)}() {\n    return ${uncap(
              e.name
            )};\n  }\n`
        )
        .join(`\n  `)
        .value();
      this.application.TestDataSetters = _.chain(this.model.entities)
        .map(
          (e) =>
            `public void set${cap(e.name)}(${cap(e.name)} ${uncap(
              e.name
            )}) {\n    this.${uncap(e.name)} = ${uncap(e.name)};\n  }\n`
        )
        .join(`\n  `)
        .value();
      const toStringAppenders = _.chain(this.model.entities)
        .map(
          (e) =>
            `sb.append(" ${uncap(e.name)}=").append(${uncap(
              e.name
            )}).append("\\n");`
        )
        .join(`\n    `)
        .value();
      this.application.TestDataToString = `@Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("TestData {\\n");
    ${toStringAppenders}
    sb.append('}');
    return sb.toString();
  }\n`;
    }
  }

  _properties(entity) {
    return props(entity, this.application);
  }

  _gradleDependency(scope, dependency) {
    return addGradleDependency(this.fs, scope, dependency);
  }

  _annotation(classFile, content) {
    return addAnnotation(this.fs, classFile, content);
  }

  writing() {
    this._gradleDependency(
      "implementation",
      "org.springframework.boot:spring-boot-starter-data-r2dbc"
    );
    this._gradleDependency("implementation", "io.r2dbc:r2dbc-postgresql");
    this._gradleDependency("testImplementation", "io.r2dbc:r2dbc-h2");

    const packageDir = this.application.packageFolder;
    this._annotation(
      `src/main/java/${packageDir}/${this.applicationName}Application.java`,
      `@EnableConfigurationProperties(${this.application.app.package}.db.DatabaseMigrationProperties.class)`
    );
    this.template("docker-compose.yml.ejs");
    this.template(
      "src/main/java/package/db/DatabaseConfiguration.java.ejs",
      `src/main/java/${packageDir}/db/DatabaseConfiguration.java`
    );
    this.template(
      "src/main/java/package/db/DatabaseMigrationProperties.java.ejs",
      `src/main/java/${packageDir}/db/DatabaseMigrationProperties.java`
    );
    this.template(
      "src/main/java/package/exception/ExceptionHandler.java.ejs",
      `src/main/java/${packageDir}/exception/ExceptionHandler.java`
    );
    this.template(
      "src/main/java/package/exception/NotFoundException.java.ejs",
      `src/main/java/${packageDir}/exception/NotFoundException.java`
    );
    this.template(
      "src/main/java/package/util/FluxUtils.java.ejs",
      `src/main/java/${packageDir}/util/FluxUtils.java`
    );
    this.template(
      "src/test/java/package/ControllerTest.java.ejs",
      `src/test/java/${packageDir}/ControllerTest.java`
    );
    this.template(
      "src/test/java/package/TestData.java.ejs",
      `src/test/java/${packageDir}/TestData.java`
    );
    this.template(
      "src/test/java/package/TestDataSetup.java.ejs",
      `src/test/java/${packageDir}/TestDataSetup.java`
    );
    this.template("src/main/resources/db/migration/V0__init.sql.ejs");
    this.template("src/test/resources/db/migration/V0__cleanup.sql.ejs");
    injectYaml(this.fs, "src/main/resources/application.yml", "spring.r2dbc", {
      url: `r2dbc:postgresql://postgresdb:5432/${this.application.app.name}`,
      username: "dbuser",
      password: "dbpassword",
      pool: {
        "initial-size": 2,
        "max-size": 10,
      },
    });
    injectYaml(
      this.fs,
      "src/main/resources/application.yml",
      "application.db",
      { "init-script": "db/migration/V0__init.sql", "clean-script": "" }
    );
    injectYaml(
      this.fs,
      "src/test/resources/application-test.yml",
      "spring.r2dbc",
      {
        url: `r2dbc:h2:mem:///${this.application.app.name}`,
        pool: { "initial-size": 4, "max-size": 4 },
      }
    );
    injectYaml(
      this.fs,
      "src/test/resources/application-test.yml",
      "application.db",
      {
        "init-script": "db/migration/V0__init.sql",
        "clean-script": "db/migration/V0__cleanup.sql",
      }
    );
    injectYaml(
      this.fs,
      "src/test/resources/application-test.yml",
      "logging.level",
      { "io.r2dbc.h2.client": "DEBUG" }
    );

    if (this.model && this.model.entities) {
      this.model.entities.forEach((entity) => {
        const entityProps = this._properties(entity);
        this.template(
          "src/main/java/package/Entity.java.ejs",
          `src/main/java/${packageDir}/${cap(entity.name)}.java`,
          entityProps
        );
        this.template(
          "src/main/java/package/EntityRepository.java.ejs",
          `src/main/java/${packageDir}/${cap(entity.name)}Repository.java`,
          entityProps
        );
        this.template(
          "src/main/java/package/EntityController.java.ejs",
          `src/main/java/${packageDir}/${cap(entity.name)}Controller.java`,
          entityProps
        );
        this.template(
          "src/test/java/package/EntityControllerTest.java.ejs",
          `src/test/java/${packageDir}/${cap(entity.name)}ControllerTest.java`,
          entityProps
        );
      });
    }
  }
};
