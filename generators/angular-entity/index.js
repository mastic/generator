"use strict";
const _ = require("lodash");
const BaseGenerator = require("../base-generator");
const pin = require("../pin-utils");
const str = require("../string-utils");
const { pluralize } = require("inflected");
const entityProps = require("../angular/entity-utils").properties;
const enumProps = require("../enum-utils").properties;
const {
  injectHtml,
  addNpmDependency,
  addNpmDevDependency,
  addToRoutingModule,
  addToSharedModule,
  addToAppModuleImports,
  importToAppModule,
  angularImport,
} = pin;
const route = str.endpoint;
const cap = str.camelCapitalize;
const uncap = str.camelUncapitalize;
const titelize = str.title;
const { endpoint } = str;

module.exports = class extends BaseGenerator {
  writing() {
    if (this.model && this.model.entities) {
      addNpmDependency(this.fs, "@ngrx/effects", "10.0.1");
      addNpmDependency(this.fs, "@ngrx/entity", "10.0.1");
      addNpmDependency(this.fs, "@ngrx/store", "10.0.1");
      addNpmDependency(this.fs, "@ngrx/store-devtools", "10.0.1");
      addNpmDependency(this.fs, "@angular/flex-layout", "11.0.0-beta.33");
      addNpmDependency(this.fs, "@angular/cdk", "11.0.0");
      addNpmDependency(this.fs, "@angular/material", "11.0.0");
      addNpmDevDependency(this.fs, "@ngrx/schematics", "10.0.1");
      addNpmDevDependency(this.fs, "@types/uuid", "8.3.0");
      addNpmDevDependency(this.fs, "jasmine-marbles", "0.8.1");
      addToSharedModule(this.fs, "HttpClientModule", "@angular/common/http");
      addToSharedModule(this.fs, "MatButtonModule", "@angular/material/button");
      addToSharedModule(
        this.fs,
        "MatDividerModule",
        "@angular/material/divider"
      );
      addToSharedModule(this.fs, "MatListModule", "@angular/material/list");
      addToSharedModule(
        this.fs,
        "MatFormFieldModule",
        "@angular/material/form-field"
      );
      addToSharedModule(this.fs, "MatInputModule", "@angular/material/input");
      addToSharedModule(this.fs, "MatTableModule", "@angular/material/table");
      addToSharedModule(
        this.fs,
        "MatExpansionModule",
        "@angular/material/expansion"
      );
      addToSharedModule(this.fs, "MatCardModule", "@angular/material/card");
      addToSharedModule(
        this.fs,
        "MatCheckboxModule",
        "@angular/material/checkbox"
      );
      addToSharedModule(this.fs, "MatDialogModule", "@angular/material/dialog");
      addToSharedModule(
        this.fs,
        "MatSlideToggleModule",
        "@angular/material/slide-toggle"
      );
      addToSharedModule(this.fs, "MatSelectModule", "@angular/material/select");
      importToAppModule(this.fs, "StoreModule", "@ngrx/store");
      importToAppModule(this.fs, "EffectsModule", "@ngrx/effects");
      importToAppModule(this.fs, "StoreDevtoolsModule", "@ngrx/store-devtools");
      importToAppModule(this.fs, "reducers, metaReducers", "./reducers");
      importToAppModule(this.fs, "environment", "../environments/environment");

      this.template("src/app/reducers/common.ts.ejs");
      this.template("src/app/reducers/debug.ts.ejs");
      this.template(
        "src/app/reducers/index.ts.ejs",
        "src/app/reducers/index.ts",
        {
          ReducerImports: _.chain(this.model.entities)
            .map(
              (e) =>
                `import * as from${cap(e.name)} from '../${endpoint(
                  e.name
                )}/${endpoint(e.name)}.reducer';`
            )
            .join(`\n`)
            .value(),
          ReducerStates: _.chain(this.model.entities)
            .map(
              (e) =>
                `[from${cap(e.name)}.${pluralize(
                  uncap(e.name)
                )}FeatureKey]: from${cap(e.name)}.State;`
            )
            .join(`\n  `)
            .value(),
          ReducerMaps: _.chain(this.model.entities)
            .map(
              (e) =>
                `[from${cap(e.name)}.${pluralize(
                  uncap(e.name)
                )}FeatureKey]: from${cap(e.name)}.reducer,`
            )
            .join(`\n  `)
            .value(),
        }
      );

      if (this.model.enums) {
        this.model.enums.forEach((enm) => {
          if (enm && enm.name) {
            const props = enumProps(enm);
            this.template(
              `src/app/enum/enum.ts.ejs`,
              `src/app/${route(enm.name)}/${route(enm.name)}.ts`,
              props
            );
          }
        });
      }

      this.model.entities.forEach((entity) => {
        if (entity && entity.name) {
          const props = entityProps(entity);
          this.addSideNavigationLink(entity);
          this.addWelcomeLink(entity);
          this.addEntityToRoutingModule(entity);
          this.entityComponent(entity, props, "-delete-dialog");
          this.entityComponent(entity, props, "-form");
          this.entityComponent(entity, props, "-update");
          this.entityComponent(entity, props, "");
          this.template(
            `src/app/entity/entity.actions.ts.ejs`,
            `src/app/${route(entity.name)}/${route(entity.name)}.actions.ts`,
            props
          );
          this.template(
            `src/app/entity/entity.effects.ts.ejs`,
            `src/app/${route(entity.name)}/${route(entity.name)}.effects.ts`,
            props
          );
          this.template(
            `src/app/entity/entity.effects.spec.ts.ejs`,
            `src/app/${route(entity.name)}/${route(
              entity.name
            )}.effects.spec.ts`,
            props
          );
          this.template(
            `src/app/entity/entity.model.ts.ejs`,
            `src/app/${route(entity.name)}/${route(entity.name)}.model.ts`,
            props
          );
          if (
            !_.chain(entity.fields)
              .filter((f) => f.isEnum())
              .isEmpty()
              .value()
          ) {
            const stmt = _.chain(entity.fields)
              .filter((f) => f.isEnum())
              .map(
                (f) =>
                  `import { ${cap(f.type)} } from '../${route(f.type)}/${route(
                    f.type
                  )}';`
              )
              .join(`\n`)
              .value();
            const file = `src/app/${route(entity.name)}/${route(
              entity.name
            )}.model.ts`;
            angularImport(this.fs, file, stmt);
          }

          this.template(
            `src/app/entity/entity.module.ts.ejs`,
            `src/app/${route(entity.name)}/${route(entity.name)}.module.ts`,
            props
          );
          this.template(
            `src/app/entity/entity.reducer.ts.ejs`,
            `src/app/${route(entity.name)}/${route(entity.name)}.reducer.ts`,
            props
          );
          this.template(
            `src/app/entity/entity.reducer.spec.ts.ejs`,
            `src/app/${route(entity.name)}/${route(
              entity.name
            )}.reducer.spec.ts`,
            props
          );
          this.template(
            `src/app/entity/entity.selectors.ts.ejs`,
            `src/app/${route(entity.name)}/${route(entity.name)}.selectors.ts`,
            props
          );
          this.template(
            `src/app/entity/entity.selectors.spec.ts.ejs`,
            `src/app/${route(entity.name)}/${route(
              entity.name
            )}.selectors.spec.ts`,
            {
              ...props,
              OtherEntityStates: _.chain(this.model.entities)
                .map((e) => e.name)
                .filter((n) => n !== entity.name)
                .map((n) => pluralize(uncap(n)))
                .map((n) => `${n}: {\n    ids: [],\n    entities: {}\n  },`)
                .join(`\n  `)
                .value(),
            }
          );
          this.template(
            `src/app/entity/entity.service.ts.ejs`,
            `src/app/${route(entity.name)}/${route(entity.name)}.service.ts`,
            props
          );
          this.template(
            `src/app/entity/entity.service.spec.ts.ejs`,
            `src/app/${route(entity.name)}/${route(
              entity.name
            )}.service.spec.ts`,
            props
          );

          importToAppModule(
            this.fs,
            `${cap(entity.name)}Effects`,
            `./${route(entity.name)}/${route(entity.name)}.effects`
          );
          importToAppModule(
            this.fs,
            `${cap(entity.name)}Module`,
            `./${route(entity.name)}/${route(entity.name)}.module`
          );
          addToAppModuleImports(this.fs, `${cap(entity.name)}Module`);
        }

        const effects =
          this.model.entities
            .map((entity) => entity.name)
            .map((name) => `\n      ${cap(name)}Effects,`)
            .reduce((str, name) => `${str}${name}`) + `\n    `;
        addToAppModuleImports(this.fs, `EffectsModule.forRoot([${effects}])`);
        addToAppModuleImports(
          this.fs,
          "StoreModule.forRoot(reducers, { metaReducers })"
        );
        addToAppModuleImports(
          this.fs,
          "StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production })"
        );
      });
    }
  }

  entityComponent(entity, entityProps, prefix) {
    if (entity && entity.name) {
      this.template(
        `src/app/entity/entity${prefix}.component.html.ejs`,
        `src/app/${route(entity.name)}/${route(
          entity.name
        )}${prefix}.component.html`,
        entityProps
      );
      this.template(
        `src/app/entity/entity${prefix}.component.sass.ejs`,
        `src/app/${route(entity.name)}/${route(
          entity.name
        )}${prefix}.component.sass`,
        entityProps
      );
      this.template(
        `src/app/entity/entity${prefix}.component.ts.ejs`,
        `src/app/${route(entity.name)}/${route(
          entity.name
        )}${prefix}.component.ts`,
        entityProps
      );
      this.template(
        `src/app/entity/entity${prefix}.component.spec.ts.ejs`,
        `src/app/${route(entity.name)}/${route(
          entity.name
        )}${prefix}.component.spec.ts`,
        entityProps
      );
    }
  }

  addEntityToRoutingModule(entity) {
    if (entity && entity.name) {
      const entityName = route(entity.name);
      const entityNameCap = cap(entity.name);
      return (
        addToRoutingModule({
          fs: this.fs,
          componentName: `${entityNameCap}Component`,
          componentPath: `./${entityName}/${entityName}.component`,
          routePath: entityName,
        }) &&
        addToRoutingModule({
          fs: this.fs,
          componentName: `${entityNameCap}UpdateComponent`,
          componentPath: `./${entityName}/${entityName}-update.component`,
          routePath: `${entityName}/:id`,
        })
      );
    }
  }

  addSideNavigationLink(entity) {
    if (entity) {
      injectHtml({
        fs: this.fs,
        file: "src/app/side-navigation/side-navigation.component.html",
        container: "mat-nav-list",
        content: `  <a mat-list-item [routerLink]="['/${route(entity.name)}']">
    <mat-icon aria-hidden="false" class="right-space">article</mat-icon>
    ${titelize(entity.name)}
  </a>\n`,
      });
    }
  }

  addWelcomeLink(entity) {
    if (entity) {
      injectHtml({
        fs: this.fs,
        file: "src/app/welcome/welcome.component.html",
        container: "div",
        content: `    <button [routerLink]="['/${route(entity.name)}']"
            mat-raised-button appElevate class="entity-button">
      <mat-icon aria-hidden="false" class="right-space">article</mat-icon>
      ${titelize(entity.name)}
    </button>\n\n`,
      });
    }
  }
};
