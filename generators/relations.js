"use strict";

const OneToOne = "OneToOne";
const OneToMany = "OneToMany";
const ManyToOne = "ManyToOne";
const ManyToMany = "ManyToMany";

module.exports = {
  OneToOne,
  OneToMany,
  ManyToOne,
  ManyToMany,
};
