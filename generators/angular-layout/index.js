"use strict";
const BaseGenerator = require("../base-generator");
const pin = require("../pin-utils");
const { title } = require("../string-utils");
const {
  addNpmDependency,
  addToSharedModule,
  addToAppModuleDeclarations,
  addToRoutingModule,
  appendToRoutingModule,
  addRedirectToRoutingModule,
  injectHtml,
  addStylesheet,
} = pin;

module.exports = class extends BaseGenerator {
  constructor(args, opts) {
    super(args, opts);
    this.staticFiles = [];
  }

  writing() {
    this.copy(this.staticFiles);
    const props = {
      ...this.application,
      AppTitle: title(this.application.app.name),
    };

    this.template("src/app/elevate.directive.spec.ts.ejs");
    this.template("src/app/elevate.directive.ts.ejs");
    this.template("src/app/error/page-not-found.component.html");
    this.template("src/app/error/page-not-found.component.sass");
    this.template("src/app/error/page-not-found.component.spec.ts.ejs");
    this.template("src/app/error/page-not-found.component.ts.ejs");
    this.template(
      "src/app/header/header.component.html.ejs",
      "src/app/header/header.component.html",
      props
    );
    this.template("src/app/header/header.component.sass");
    this.template("src/app/header/header.component.spec.ts.ejs");
    this.template("src/app/header/header.component.ts.ejs");
    this.template("src/app/side-navigation/side-navigation.component.html");
    this.template("src/app/side-navigation/side-navigation.component.sass");
    this.template(
      "src/app/side-navigation/side-navigation.component.spec.ts.ejs"
    );
    this.template("src/app/side-navigation/side-navigation.component.ts.ejs");
    this.template("src/app/welcome/welcome.component.html");
    this.template("src/app/welcome/welcome.component.sass");
    this.template("src/app/welcome/welcome.component.spec.ts.ejs");
    this.template("src/app/welcome/welcome.component.ts.ejs");
    this.template("src/app/app.component.spec.ts.ejs");
    this.template("src/app/app.component.ts.ejs");

    addToRoutingModule({
      fs: this.fs,
      componentName: `WelcomeComponent`,
      componentPath: `./welcome/welcome.component`,
      routePath: `welcome`,
    });
    addRedirectToRoutingModule(this.fs, ``, `/welcome`);
    appendToRoutingModule(
      this.fs,
      `PageNotFoundComponent`,
      `./error/page-not-found.component`,
      `**`
    );

    addNpmDependency(this.fs, "@angular/flex-layout", "10.0.0-beta.32");
    addNpmDependency(this.fs, "@angular/cdk", "10.2.0");
    addNpmDependency(this.fs, "@angular/material", "10.2.0");
    addToSharedModule(this.fs, "FlexLayoutModule", "@angular/flex-layout");
    addToSharedModule(this.fs, "MatToolbarModule", "@angular/material/toolbar");
    addToSharedModule(this.fs, "MatSidenavModule", "@angular/material/sidenav");
    addToAppModuleDeclarations(
      this.fs,
      "WelcomeComponent",
      "./welcome/welcome.component"
    );
    addToAppModuleDeclarations(
      this.fs,
      "HeaderComponent",
      "./header/header.component"
    );
    addToAppModuleDeclarations(
      this.fs,
      "SideNavigationComponent",
      "./side-navigation/side-navigation.component"
    );
    addToAppModuleDeclarations(
      this.fs,
      "PageNotFoundComponent",
      "./error/page-not-found.component"
    );
    addToAppModuleDeclarations(
      this.fs,
      "ElevateDirective",
      "./elevate.directive"
    );

    injectHtml({
      fs: this.fs,
      file: "src/app/app.component.html",
      container: "div.container",
      content: `\n  <app-header (toggleSideNavigation)="sidemenu.toggle()"></app-header>

    <mat-sidenav-container class="sidenav-container">
      <mat-sidenav #sidemenu
        fxLayout="column"
        fixedInViewport="true"
        [mode]="(sideNavMode | async)!"
        [opened]="sideNavOpen | async"
        [fixedTopGap]="topGap | async" >
        <app-side-navigation></app-side-navigation>
      </mat-sidenav>

      <mat-sidenav-content fxFlexFill>
        <router-outlet></router-outlet>
      </mat-sidenav-content>
    </mat-sidenav-container>\n`,
      replace: true,
    });
    addStylesheet(
      this.fs,
      "./node_modules/@angular/material/prebuilt-themes/indigo-pink.css"
    );
  }
};
