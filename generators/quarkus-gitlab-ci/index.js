"use strict";
const BaseGenerator = require("../base-generator");

module.exports = class extends BaseGenerator {
  writing() {
    this.template(".gitlab-ci.yml.ejs");
  }
};
