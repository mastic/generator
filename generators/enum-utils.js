"use strict";
const _ = require("lodash");
const str = require("./string-utils");
const cap = str.camelCapitalize;

function properties(enm, properties) {
  if (!properties) {
    properties = {};
  }

  return {
    ...properties,
    ClassName: cap(enm.name),
    Values: _.chain(enm.values)
      .map((val) => `  ${val},`)
      .join(`\n`)
      .value(),
  };
}

module.exports = {
  properties,
};
