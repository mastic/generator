"use strict";
const BaseGenerator = require("../base-generator");
const props = require("../enum-utils").properties;
const str = require("../string-utils");
const cap = str.camelCapitalize;

module.exports = class extends BaseGenerator {
  constructor(args, opts) {
    super(args, opts);
    this.staticFiles = [
      ".dockerignore",
      ".gitignore",
      "gradlew.bat",
      "gradlew",
      "gradle/wrapper/gradle-wrapper.properties",
      "gradle/wrapper/gradle-wrapper.jar",
    ];
  }

  properties() {
    this.application.appName = cap(this.application.app.name);
  }

  _properties(enm) {
    return props(enm, this.application);
  }

  writing() {
    this.copy(this.staticFiles);
    this.template("build.gradle.ejs");
    this.template("settings.gradle.ejs");
    this.template("README.md.ejs");
    this.template("src/main/resources/application.yml.ejs");
    this.template("src/test/resources/application-test.yml.ejs");
    this.template(
      "src/main/java/package/Application.java.ejs",
      `src/main/java/${this.application.packageFolder}/${cap(
        this.application.app.name
      )}Application.java`
    );
    this.template(
      "src/test/java/package/ApplicationTest.java.ejs",
      `src/test/java/${this.application.packageFolder}/${cap(
        this.application.app.name
      )}ApplicationTest.java`
    );

    const packageDir = this.application.packageFolder;
    if (this.model && this.model.enums) {
      this.model.enums.forEach((enm) => {
        const enumProps = this._properties(enm);
        this.template(
          "src/main/java/package/Enum.java.ejs",
          `src/main/java/${packageDir}/${cap(enm.name)}.java`,
          enumProps
        );
      });
    }
  }
};
