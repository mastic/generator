"use strict";
const _ = require("lodash");
const { pluralize } = require("inflected");
const str = require("../string-utils");
const type = require("../type-utils");
const { FieldVisitor } = require("../data-type");
const { OneToOne, ManyToOne, ManyToMany, OneToMany } = require("../relations");
const { endpoint } = require("../string-utils");

const cap = str.camelCapitalize;
const uncap = str.camelUncapitalize;
const endpoints = str.pluralEndpoint;
const tableName = str.snakeCapitalize;
const colName = str.snakeCapitalize;
const val = type.testValue;
const valUpdated = type.testValueUpdated;

class FieldType extends FieldVisitor {
  acceptEnum(field) {
    return field.type;
  }

  acceptString(_field) {
    return `String`;
  }

  acceptBoolean(_field) {
    return `Boolean`;
  }

  acceptLong(_field) {
    return "Long";
  }

  acceptInteger(_field) {
    return "Integer";
  }

  acceptDouble(_field) {
    return `Double`;
  }

  acceptDatetime(_field) {
    return `Instant`;
  }

  acceptRelation(_field) {
    return `Long`;
  }
}

function fieldType(field) {
  return field.accept(new FieldType());
}

function columnName(field) {
  return field.isRelation() ? colName(`${field.name}_ID`) : colName(field.name);
}

function fieldName(field) {
  return field.isRelation() ? uncap(`${field.name}Id`) : uncap(field.name);
}

function properties(entity, properties) {
  if (!properties) {
    properties = {};
  }

  return {
    ...properties,
    ClassName: cap(entity.name),
    ClassNames: pluralize(cap(entity.name)),
    TableName: tableName(entity.name),
    ObjectName: uncap(entity.name),
    ObjectNames: pluralize(uncap(entity.name)),
    EndPoint: endpoints(entity.name),
    Imports: type.javaImports(entity.fields),
    Fields: _.chain(entity.fields)
      .filter(
        (f) =>
          !f.isRelation() ||
          f.getRelation() === ManyToOne ||
          f.getRelation() === OneToOne
      )
      .map((f) => {
        const opt = f.isOptional() ? `\n  @Nullable` : `\n  @NotNull`;
        return `${opt}\n  @Column("${columnName(
          f
        )}")\n  @JsonProperty\n  private ${fieldType(f)} ${fieldName(f)};`;
      })
      .join(`\n`)
      .value(),
    Getters: _.chain(entity.fields)
      .filter(
        (f) =>
          !f.isRelation() ||
          f.getRelation() === ManyToOne ||
          f.getRelation() === OneToOne
      )
      .map(
        (f) =>
          `\n  public ${fieldType(f)} get${cap(
            fieldName(f)
          )}() { return ${fieldName(f)}; }`
      )
      .join(`\n`)
      .value(),
    Setters: _.chain(entity.fields)
      .filter(
        (f) =>
          !f.isRelation() ||
          f.getRelation() === ManyToOne ||
          f.getRelation() === OneToOne
      )
      .map(
        (f) =>
          `\n  public void set${cap(fieldName(f))}(${fieldType(f)} ${fieldName(
            f
          )}) { this.${fieldName(f)} = ${fieldName(f)}; }`
      )
      .join(`\n`)
      .value(),
    FieldsEquals: _.chain(entity.fields)
      .filter(
        (f) =>
          !f.isRelation() ||
          f.getRelation() === ManyToOne ||
          f.getRelation() === OneToOne
      )
      .map(
        (f) =>
          `&& Objects.equals(${fieldName(f)}, ${uncap(entity.name)}.${fieldName(
            f
          )})`
      )
      .join(`\n        `)
      .value(),
    FieldsToString: _.chain(entity.fields)
      .filter(
        (f) =>
          !f.isRelation() ||
          f.getRelation() === ManyToOne ||
          f.getRelation() === OneToOne
      )
      .map((f) => `sb.append(", ${fieldName(f)}=").append(${fieldName(f)});`)
      .join(`\n    `)
      .value(),
    SetFieldTestValues: _.chain(entity.fields)
      .filter(
        (f) =>
          !f.isRelation() ||
          f.getRelation() === ManyToOne ||
          f.getRelation() === OneToOne
      )
      .map((f) => {
        let setter = `set${cap(f.name)}`;
        if (f.isRelation()) setter = `set${cap(f.name)}Id`;
        return `entity.${setter}(${val(f)});`;
      })
      .join(`\n    `)
      .value(),
    AssertFieldTestValues: _.chain(entity.fields)
      .filter(
        (f) =>
          !f.isRelation() ||
          f.getRelation() === ManyToOne ||
          f.getRelation() === OneToOne
      )
      .map((f) => {
        let getter = `get${cap(f.name)}`;
        if (f.isRelation()) getter = `get${cap(f.name)}Id`;
        return `assertEquals(${val(f)}, created.${getter}());`;
      })
      .join(`\n    `)
      .value(),
    SetFieldTestValuesUpdated: _.chain(entity.fields)
      .filter(
        (f) =>
          !f.isRelation() ||
          f.getRelation() === ManyToOne ||
          f.getRelation() === OneToOne
      )
      .map((f) => {
        let setter = `set${cap(f.name)}`;
        if (f.isRelation()) setter = `set${cap(f.name)}Id`;
        return `entity.${setter}(${valUpdated(f)});`;
      })
      .join(`\n    `)
      .value(),
    AssertFieldTestValuesUpdated: _.chain(entity.fields)
      .filter(
        (f) =>
          !f.isRelation() ||
          f.getRelation() === ManyToOne ||
          f.getRelation() === OneToOne
      )
      .map((f) => {
        let getter = `get${cap(f.name)}`;
        if (f.isRelation()) getter = `get${cap(f.name)}Id`;
        return `assertEquals(${valUpdated(f)}, updated.${getter}());`;
      })
      .join(`\n    `)
      .value(),
    RelationalFinders: _.chain(entity.fields)
      .filter(
        (f) => f.getRelation() === ManyToOne || f.getRelation() === OneToOne
      )
      .map(
        (f) =>
          `  Flux<${cap(entity.name)}> findBy${cap(
            fieldName(f)
          )}(Long ${fieldName(f)});`
      )
      .join(`\n`)
      .value(),
    JavaImports: _.chain(entity.fields)
      .filter(
        (f) => f.getRelation() === ManyToOne || f.getRelation() === OneToOne
      )
      .map((_f) => `import reactor.core.publisher.Flux;`)
      .sortedUniq()
      .join(`\n`)
      .value(),
    RelationalRepositoryFields: _.chain(entity.fields)
      .filter((f) => f.isRelation())
      .map(
        (f) =>
          `private final ${cap(f.type)}Repository ${uncap(f.name)}Repository;`
      )
      .join(`\n  `)
      .value(),
    RelationalRepositoryParams: _.chain(entity.fields)
      .filter((f) => f.isRelation())
      .map((f) => `, ${cap(f.type)}Repository ${uncap(f.name)}Repository`)
      .join(``)
      .value(),
    RelationalRepositoryAssignments: _.chain(entity.fields)
      .filter((f) => f.isRelation())
      .map(
        (f) => `this.${uncap(f.name)}Repository = ${uncap(f.name)}Repository;`
      )
      .join(`\n    `)
      .value(),
    ManyRelations: _.chain(entity.fields)
      .filter(
        (f) => f.getRelation() === ManyToMany || f.getRelation() === OneToMany
      )
      .map((f) => f.name)
      .value(),
    SingleRelations: _.chain(entity.fields)
      .filter(
        (f) => f.getRelation() === ManyToOne || f.getRelation() === OneToOne
      )
      .map((f) => f.name)
      .value(),
    RelationEndpoints: _.chain(entity.fields)
      .filter((f) => f.isRelation())
      .keyBy("name")
      .mapValues((f) => endpoint(f.name))
      .value(),
    RelationObjects: _.chain(entity.fields)
      .filter((f) => f.isRelation())
      .keyBy("name")
      .mapValues((f) => uncap(f.name))
      .value(),
    RelationTypes: _.chain(entity.fields)
      .filter((f) => f.isRelation())
      .keyBy("name")
      .mapValues((f) => cap(f.type))
      .value(),
    RelationNames: _.chain(entity.fields)
      .filter((f) => f.isRelation())
      .keyBy("name")
      .mapValues((f) => cap(f.name))
      .value(),
    RelationSetFieldTestValues: _.chain(entity.fields)
      .filter((f) => f.isRelation())
      .keyBy("name")
      .mapValues((f) => {
        if (f.typeEntity) {
          return _.chain(f.typeEntity.fields)
            .filter((rf) => !rf.isRelation())
            .map((rf) => `${uncap(f.name)}.set${cap(rf.name)}(${val(rf)});`)
            .join(`\n    `)
            .value();
        }

        return ``;
      })
      .value(),
    RelationSetRelationFields: _.chain(entity.fields)
      .filter((f) => f.isRelation())
      .keyBy("name")
      .mapValues((f) => {
        if (f.typeEntity) {
          return _.chain(f.typeEntity.fields)
            .filter(
              (rf) =>
                rf.getRelation() === ManyToOne || rf.getRelation() === OneToOne
            )
            .map(
              (rf) =>
                `${uncap(f.name)}.set${cap(rf.name)}Id(data.get${cap(
                  rf.typeEntity.name
                )}().getId());`
            )
            .join(`\n    `)
            .value();
        }

        return ``;
      })
      .value(),
    RelationAssertFieldTestValues: _.chain(entity.fields)
      .filter((f) => f.isRelation())
      .keyBy("name")
      .mapValues((f) => {
        if (f.typeEntity) {
          return _.chain(f.typeEntity.fields)
            .filter((rf) => !rf.isRelation())
            .map(
              (rf) => `assertEquals(${val(rf)}, created.get${cap(rf.name)}());`
            )
            .join(`\n    `)
            .value();
        }

        return ``;
      })
      .value(),
    RelationSetFieldTestValuesUpdated: _.chain(entity.fields)
      .filter((f) => f.isRelation())
      .keyBy("name")
      .mapValues((f) => {
        if (f.typeEntity) {
          return _.chain(f.typeEntity.fields)
            .filter((rf) => !rf.isRelation())
            .map(
              (rf) => `${uncap(f.name)}.set${cap(rf.name)}(${valUpdated(rf)});`
            )
            .join(`\n    `)
            .value();
        }

        return ``;
      })
      .value(),
    RelationAssertFieldTestValuesUpdated: _.chain(entity.fields)
      .filter((f) => f.isRelation())
      .keyBy("name")
      .mapValues((f) => {
        if (f.typeEntity) {
          return _.chain(f.typeEntity.fields)
            .filter((rf) => !rf.isRelation())
            .map(
              (rf) =>
                `assertEquals(${valUpdated(rf)}, updated.get${cap(rf.name)}());`
            )
            .join(`\n    `)
            .value();
        }

        return ``;
      })
      .value(),
  };
}

module.exports = {
  properties,
  columnName,
};
