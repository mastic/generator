"use strict";
const BaseGenerator = require("../base-generator");
const str = require("../string-utils");
const pin = require("../pin-utils");
const props = require("../quarkus/entity-utils").properties;

const cap = str.camelCapitalize;
const { addGradleDependency, addApplicationProperty } = pin;

module.exports = class extends BaseGenerator {
  _properties(entity) {
    return props(entity, this.application);
  }

  _gradleDependency(scope, dependency) {
    return addGradleDependency(this.fs, scope, dependency);
  }

  _applicationProperty(name, value) {
    return addApplicationProperty(this.fs, name, value);
  }

  writing() {
    this._gradleDependency(
      "implementation",
      "io.quarkus:quarkus-reactive-pg-client"
    );
    this._gradleDependency("implementation", "io.quarkus:quarkus-vertx");
    this._gradleDependency("implementation", "io.quarkus:quarkus-resteasy");
    this._gradleDependency(
      "implementation",
      "io.quarkus:quarkus-resteasy-jackson"
    );
    this._gradleDependency(
      "implementation",
      "io.quarkus:quarkus-resteasy-mutiny"
    );
    this._gradleDependency(
      "implementation",
      "io.netty:netty-transport-native-epoll"
    );
    this._gradleDependency(
      "implementation",
      "org.apache.commons:commons-lang3"
    );

    this._applicationProperty("quarkus.vertx.prefer-native-transport", "true");
    this._applicationProperty("quarkus.http.so-reuse-port", "true");
    this._applicationProperty("quarkus.datasource.db-kind", "postgresql");
    this._applicationProperty("quarkus.datasource.username", "dbuser");
    this._applicationProperty("quarkus.datasource.password", "dbpassword");
    this._applicationProperty(
      "quarkus.datasource.reactive.url",
      `postgresql://postgresdb:5432/${this.application.app.name}`
    );

    const packageDir = this.application.packageFolder;
    this.template("docker-compose.yml.ejs");
    this.template(
      "src/main/java/package/ReactiveCrudRepository.java.ejs",
      `src/main/java/${packageDir}/ReactiveCrudRepository.java`
    );
    if (this.model && this.model.entities) {
      this.model.entities.forEach((entity) => {
        const entityProps = this._properties(entity);
        this.template(
          "src/main/java/package/SerializationCustomizer.java.ejs",
          `src/main/java/${packageDir}/SerializationCustomizer.java`,
          entityProps
        );
        this.template(
          "src/main/java/package/Entity.java.ejs",
          `src/main/java/${packageDir}/${cap(entity.name)}.java`,
          entityProps
        );
        this.template(
          "src/main/java/package/EntityRepository.java.ejs",
          `src/main/java/${packageDir}/${cap(entity.name)}Repository.java`,
          entityProps
        );
        this.template(
          "src/main/java/package/EntityResource.java.ejs",
          `src/main/java/${packageDir}/${cap(entity.name)}Resource.java`,
          entityProps
        );
        this.template(
          "src/test/java/package/EntityRepositoryTest.java.ejs",
          `src/test/java/${packageDir}/${cap(entity.name)}RepositoryTest.java`,
          entityProps
        );
        this.template(
          "src/test/java/package/EntityResourceTest.java.ejs",
          `src/test/java/${packageDir}/${cap(entity.name)}ResourceTest.java`,
          entityProps
        );
        this.template(
          "src/native-test/java/package/NativeEntityResourceTest.java.ejs",
          `src/native-test/java/${packageDir}/Native${cap(
            entity.name
          )}ResourceTest.java`,
          entityProps
        );
      });
    }
  }
};
