"use strict";
const camelCase = require("camelcase");
const decamelize = require("decamelize");
const { pluralize, titleize } = require("inflected");

function camelCapitalize(string) {
  if (!string) return string;
  return camelCase(string, { pascalCase: true });
}

function camelUncapitalize(string) {
  if (!string) return string;
  return camelCase(string);
}

function endpoint(string) {
  if (!string) return string;
  return decamelize(string, "-");
}

function pluralEndpoint(string) {
  if (!string) return string;
  return pluralize(decamelize(string, "-"));
}

function snakeCapitalize(string) {
  if (!string) return string;
  return decamelize(string, "_").toLocaleUpperCase("en-US");
}

function snakeUncapitalize(string) {
  if (!string) return string;
  return decamelize(string, "_").toLocaleLowerCase("en-US");
}

function title(string) {
  if (!string) return string;
  return titleize(decamelize(string, " "));
}

function pluralTitle(string) {
  if (!string) return string;
  return titleize(decamelize(pluralize(string), " "));
}

module.exports = {
  camelCapitalize,
  camelUncapitalize,
  endpoint,
  pluralEndpoint,
  snakeCapitalize,
  snakeUncapitalize,
  title,
  pluralTitle,
};
