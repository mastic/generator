"use strict";
const Generator = require("yeoman-generator");
const {
  readConfiguration,
  writeConfiguration,
} = require("../mastic/configuration");

const isComplete = (config) =>
  config &&
  config.application &&
  config.stack &&
  config.model &&
  config.ignores;

module.exports = class extends Generator {
  constructor(args, config) {
    super(args, config);

    if (!isComplete(config)) {
      config = readConfiguration(this.fs);
    }

    this.application = config.application;
    this.stack = config.stack;
    this.model = config.model;
    this.ignores = config.ignores;
  }

  writeConfiguration() {
    return writeConfiguration(this.fs, this.log, {
      applicatioon: this.application,
      stack: this.stack,
      model: this.model,
      ignores: this.ignores,
    });
  }

  copy(files) {
    this._.forEach(files, (file) => {
      const source = this.templatePath(file);
      const destination = this.destinationPath(file);
      try {
        if (this.ignores.includes(file)) {
          this.log.info(`ignoring ${file}`);
          return;
        }

        this.fs.copy(source, destination);
      } catch (error) {
        this.log.error(`could not write static file: ${destination}`);
        this.log.error(error);
      }
    });
  }

  template(src, dest, props) {
    if (!dest) {
      dest = src;
      if (dest.indexOf(".ejs") >= 0) {
        dest = dest.substring(0, dest.indexOf(".ejs"));
      }
    }

    if (!props) {
      props = this.application;
    }

    const source = this.templatePath(src);
    const destination = this.destinationPath(dest);

    if (!this.fs.exists(source)) {
      this.log.error(`template missing: ${source}`);
      return;
    }

    if (this.ignores.includes(dest)) {
      this.log.info(`ignoring ${dest}`);
      return;
    }

    try {
      this.fs.copyTpl(
        source,
        destination,
        {
          ...props,
          model: this.model,
        },
        {
          escape: false,
        }
      );
    } catch (error) {
      console.log(`template could not write file ${destination}: ${error}`);
    }
  }
};
