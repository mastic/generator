"use strict";

const chalk = require("chalk");
const path = require("path");
const cheerio = require("cheerio");
const serialize = require("dom-serializer").default;

function rewrite(content, pin, ...injection) {
  const lines = content.split("\n");

  let otherwiseLineIndex = -1;
  lines.forEach((line, i) => {
    if (line.includes(pin)) {
      otherwiseLineIndex = i;
    }
  });

  let spaces = 0;
  while (lines[otherwiseLineIndex].charAt(spaces) === " ") {
    spaces += 1;
  }

  let spaceStr = "";

  while ((spaces -= 1) >= 0) {
    spaceStr += " ";
  }

  lines.splice(
    otherwiseLineIndex,
    0,
    injection.map((line) => spaceStr + line).join("\n")
  );

  return lines.join("\n");
}

// eslint-disable-next-line max-params
function rewriteFile(fs, dir, file, pin, ...content) {
  dir = dir || process.cwd();
  const fullPath = path.join(dir, file);

  if (fs.exists(fullPath)) {
    const contentBefore = fs.read(fullPath);
    const contentAfter = rewrite(contentBefore, pin, content);
    fs.write(fullPath, contentAfter);
    return contentBefore !== contentAfter;
  }
}

function readJSONFile(fs, dir, file) {
  dir = dir || process.cwd();
  const fullPath = path.join(dir, file);
  return fs.readJSON(fullPath);
}

function writeJSONFile(fs, dir, file, content) {
  dir = dir || process.cwd();
  const fullPath = path.join(dir, file);
  return fs.writeJSON(fullPath, content);
}

// eslint-disable-next-line max-params
function injectContentToFile(fs, dir, file, pin, ...content) {
  try {
    return rewriteFile(fs, dir, file, pin, content);
  } catch (_) {
    const message = "File injection failed";
    console.log(
      chalk.yellow("\nUnable to find ") +
        file +
        chalk.yellow(` or missing required pin. ${message}\n`)
    );
    return false;
  }
}

function addAnnotation(fs, classFile, content) {
  const dir = process.cwd();
  const pin = "mastic-pin-annotation";
  return injectContentToFile(fs, dir, classFile, pin, content);
}

function addGradleDependency(fs, scope, dependency) {
  const dir = process.cwd();
  const file = "build.gradle";
  const pin = "mastic-pin-gradle-dependency";
  const content = `${scope} '${dependency}'`;
  return injectContentToFile(fs, dir, file, pin, content);
}

function addApplicationProperty(fs, name, value) {
  const dir = path.join(process.cwd(), "src/main/resources");
  const file = "application.properties";
  const pin = "mastic-pin-application.properties";
  const content = `${name}=${value}`;
  return injectContentToFile(fs, dir, file, pin, content);
}

function addEntityChangeLog(fs, entityName) {
  const dir = path.join(process.cwd(), "src/main/resources/db");
  const file = "changeLog.xml";
  const pin = "mastic-pin-liquibase-entities";
  const content = `<include file="db/entity_${entityName}.xml" relativeToChangelogFile="false"/>`;
  return injectContentToFile(fs, dir, file, pin, content);
}

function addToSharedModule(fs, module, namespace) {
  const dir = path.join(process.cwd(), "src/app/shared");
  const file = "shared.module.ts";
  const importStatementPin = "mastic-pin-shared-import-statements";
  const importsPin = "mastic-pin-shared-imports";
  const exportsPin = "mastic-pin-shared-exports";
  const importStatement = `import { ${module} } from '${namespace}';`;
  return (
    injectContentToFile(fs, dir, file, importStatementPin, importStatement) &&
    injectContentToFile(fs, dir, file, importsPin, `${module},`) &&
    injectContentToFile(fs, dir, file, exportsPin, `${module},`)
  );
}

function addToAppModuleDeclarations(fs, module, namespace) {
  const dir = path.join(process.cwd(), "src/app");
  const file = "app.module.ts";
  const importStatementPin = "mastic-pin-app-import-statements";
  const declarationsPin = "mastic-pin-app-declarations";
  const importStatement = `import { ${module} } from '${namespace}';`;
  return (
    injectContentToFile(fs, dir, file, importStatementPin, importStatement) &&
    injectContentToFile(fs, dir, file, declarationsPin, `${module},`)
  );
}

function importToAppModule(fs, module, namespace) {
  const dir = path.join(process.cwd(), "src/app");
  const file = "app.module.ts";
  const importStatementPin = "mastic-pin-app-import-statements";
  const importStatement = `import { ${module} } from '${namespace}';`;
  return injectContentToFile(
    fs,
    dir,
    file,
    importStatementPin,
    importStatement
  );
}

function angularImport(fs, file, importStatement) {
  const dir = process.cwd();
  const fullPath = path.join(dir, file);
  if (fs && fs.exists(fullPath)) {
    const importStatementPin = "mastic-pin-import-statements";
    return injectContentToFile(
      fs,
      dir,
      file,
      importStatementPin,
      importStatement
    );
  }
}

function fileContains(fs, dir, file, content) {
  const fullPath = path.join(dir, file);
  if (fs.exists(fullPath)) {
    const fileContent = fs.read(fullPath);
    if (fileContent && content && fileContent.includes(content)) {
      return true;
    }
  }

  return false;
}

function addToAppModuleImports(fs, importStatement) {
  const dir = path.join(process.cwd(), "src/app");
  const file = "app.module.ts";
  const importsPin = "mastic-pin-app-imports";
  if (!fileContains(fs, dir, file, `${importStatement},`)) {
    return injectContentToFile(
      fs,
      dir,
      file,
      importsPin,
      `${importStatement},`
    );
  }
}

function addToRoutingModule({
  fs,
  componentName,
  componentPath,
  routePath,
  routePin = "mastic-pin-routing-mid-route",
}) {
  const dir = path.join(process.cwd(), "src/app");
  const file = "app-routing.module.ts";
  const fullPath = path.join(dir, file);
  if (fs && fs.exists(fullPath)) {
    const importsPin = "mastic-pin-import-statements";
    const imports = `import { ${componentName} } from '${componentPath}';`;
    const routeStatement = `{ path: '${routePath}', component: ${componentName} },`;
    return (
      injectContentToFile(fs, dir, file, importsPin, imports) &&
      injectContentToFile(fs, dir, file, routePin, routeStatement)
    );
  }
}

function appendToRoutingModule(fs, componentName, componentPath, routePath) {
  return addToRoutingModule({
    fs,
    componentName,
    componentPath,
    routePath,
    routePin: "mastic-pin-routing-last-route",
  });
}

function addRedirectToRoutingModule(fs, routePath, targetPath) {
  const dir = path.join(process.cwd(), "src/app");
  const file = "app-routing.module.ts";
  const fullPath = path.join(dir, file);
  if (fs && fs.exists(fullPath)) {
    const routePin = "mastic-pin-routing-mid-route";
    const routeStatement = `{ path: '${routePath}', redirectTo: '${targetPath}', pathMatch: 'full' },`;
    return injectContentToFile(fs, dir, file, routePin, routeStatement);
  }
}

const sortObject = (o) =>
  Object.keys(o)
    .sort()
    .reduce((r, k) => ((r[k] = o[k]), r), {}); // eslint-disable-line no-return-assign, no-sequences

function addStylesheet(fs, cssPath) {
  const dir = process.cwd();
  const file = "angular.json";
  const angularJson = readJSONFile(fs, dir, file);
  if (angularJson) {
    const { projects } = angularJson;
    Object.entries(projects).forEach(([, project]) => {
      if (
        project.architect &&
        project.architect.build &&
        project.architect.build.options &&
        project.architect.build.options.styles
      ) {
        const { styles } = project.architect.build.options;
        if (styles.includes(cssPath)) {
          chalk.yellow(`Style already exists: ${JSON.stringify(styles)}`);
        } else {
          styles.push(cssPath);
        }
      }

      if (
        project.architect &&
        project.architect.test &&
        project.architect.test.options &&
        project.architect.test.options.styles
      ) {
        const { styles } = project.architect.test.options;
        if (styles.includes(cssPath)) {
          chalk.yellow(`Style already exists: ${JSON.stringify(styles)}`);
        } else {
          styles.push(cssPath);
        }
      }
    });
    return writeJSONFile(fs, dir, file, angularJson);
  }
}

function addNpmDependency(
  fs,
  dependency,
  version,
  dependencyType = "dependencies"
) {
  const dir = process.cwd();
  const file = "package.json";
  let packageJson = readJSONFile(fs, dir, file);
  if (packageJson) {
    let dependencies = packageJson[dependencyType];
    if (!dependencies) {
      dependencies = {};
      packageJson[dependencyType] = dependencies;
    }

    if (dependencies[dependency]) {
      chalk.yellow(
        `Dependency already exists: ${dependency} @ ${dependencies[dependency]}`
      );
    } else {
      dependencies[dependency] = version;
      packageJson[dependencyType] = sortObject(dependencies);
    }
  } else {
    const dependencies = {};
    dependencies[dependency] = version;
    packageJson = {};
    packageJson[dependencyType] = dependencies;
  }

  return writeJSONFile(fs, dir, file, packageJson);
}

function addNpmDevDependency(fs, dependency, version) {
  return addNpmDependency(fs, dependency, version, "devDependencies");
}

function injectHtml({ fs, file, container, content, replace = false }) {
  if (file && fs.exists(file)) {
    const html = fs.read(file);
    const $ = cheerio.load(html, {
      decodeEntities: false,
      xmlMode: true,
    });
    if (replace) {
      $(container).empty().append(content);
    } else {
      $(container).append(content);
    }

    const serializedHtml = serialize($.root());
    fs.write(file, serializedHtml);
  }
}

module.exports = {
  addGradleDependency,
  addAnnotation,
  addApplicationProperty,
  addEntityChangeLog,
  addStylesheet,
  addNpmDependency,
  addNpmDevDependency,
  addToSharedModule,
  addToAppModuleDeclarations,
  addToAppModuleImports,
  importToAppModule,
  appendToRoutingModule,
  addToRoutingModule,
  addRedirectToRoutingModule,
  injectContentToFile,
  injectHtml,
  angularImport,
};
