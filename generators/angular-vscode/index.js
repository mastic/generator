"use strict";
const BaseGenerator = require("../base-generator");

module.exports = class extends BaseGenerator {
  constructor(args, opts) {
    super(args, opts);
    this.staticFiles = [".vscode/settings.json"];
  }

  writing() {
    this.copy(this.staticFiles);
  }
};
