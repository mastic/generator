"use strict";
const _ = require("lodash");
const { pluralize } = require("inflected");
const str = require("../string-utils");
const type = require("../type-utils");
const { FieldVisitor } = require("../data-type");

const cap = str.camelCapitalize;
const uncap = str.camelUncapitalize;
const endpoints = str.pluralEndpoint;
const tableName = str.snakeCapitalize;
const fieldName = str.snakeUncapitalize;
const { javaType, dbType } = type;
const val = type.testValue;
const valUpdated = type.testValueUpdated;

class RowToJava extends FieldVisitor {
  acceptEnum(field) {
    return `${field.type}.valueOf(row.getString("${fieldName(field.name)}"))`;
  }

  acceptString(field) {
    return `row.getString("${fieldName(field.name)}")`;
  }

  acceptBoolean(field) {
    return `row.getBoolean("${fieldName(field.name)}")`;
  }

  acceptLong(field) {
    return `row.getLong("${fieldName(field.name)}")`;
  }

  acceptInteger(field) {
    return `row.getInteger("${fieldName(field.name)}")`;
  }

  acceptDouble(field) {
    return `row.getDouble("${fieldName(field.name)}")`;
  }

  acceptDatetime(field) {
    return `row.getLocalDateTime("${fieldName(
      field.name
    )}").atZone(UTC).toInstant()`;
  }

  acceptRelation(_field) {
    return `TODO`;
  }
}

function fromRow(field) {
  return field.accept(new RowToJava());
}

class JavaToRow extends FieldVisitor {
  acceptEnum(field) {
    return `.addString(entity.${field.name}.toString())`;
  }

  acceptString(field) {
    return `.addString(entity.${field.name})`;
  }

  acceptBoolean(field) {
    return `.addBoolean(entity.${field.name})`;
  }

  acceptLong(field) {
    return `.addLong(entity.${field.name})`;
  }

  acceptInteger(field) {
    return `.addInteger(entity.${field.name})`;
  }

  acceptDouble(field) {
    return `.addDouble(entity.${field.name})`;
  }

  acceptDatetime(field) {
    return `.addLocalDateTime(LocalDateTime.ofInstant(entity.${field.name}, UTC))`;
  }

  acceptRelation(_field) {
    return `TODO`;
  }
}

function toRow(field) {
  return field.accept(new JavaToRow());
}

function properties(entity, properties) {
  if (!properties) {
    properties = {};
  }

  return {
    ...properties,
    ClassName: cap(entity.name),
    ClassNames: pluralize(cap(entity.name)),
    TableName: tableName(entity.name),
    ObjectName: uncap(entity.name),
    ObjectNames: pluralize(uncap(entity.name)),
    EndPoint: endpoints(entity.name),
    Fields: _.chain(entity.fields)
      .map((f) => `\n  public ${javaType(f)} ${f.name};`)
      .join(`\n`)
      .value(),
    FieldsAsParams: _.chain(entity.fields)
      .map((f) => `, ${javaType(f)} ${f.name}`)
      .join(` `)
      .value(),
    FieldsAssign: _.chain(entity.fields)
      .map((f) => `this.${f.name} = ${f.name};`)
      .join(`\n    `)
      .value(),
    FieldIndicesAssign: _.chain(entity.fields)
      .map((f, idx) => `${fieldName(f.name)} = $${idx + 1}`)
      .join(`, `)
      .value(),
    FieldsAppend: _.chain(entity.fields)
      .map((f) => `.append("${f.name}", ${f.name})`)
      .join(`\n        `)
      .value(),
    FieldNames: _.chain(entity.fields)
      .map((f) => `, ${fieldName(f.name)}`)
      .join(``)
      .value(),
    FieldNamesList: _.chain(entity.fields)
      .map((f) => `${fieldName(f.name)}`)
      .join(`, `)
      .value(),
    FieldIndices: _.range(1, entity.fields.length + 1)
      .map((f) => `$${f}`)
      .join(`, `),
    TableFields: _.chain(entity.fields)
      .map((f) => `, ${fieldName(f.name)} ${dbType(f)}`)
      .join(``),
    FieldCount: entity.fields.length,
    FieldsTuple: _.chain(entity.fields)
      .map((f) => `\n        ${toRow(f)}`)
      .join(``)
      .value(),
    RowFields: _.chain(entity.fields)
      .map((f) => `,\n        ${fromRow(f)}`)
      .join(``)
      .value(),
    FieldTestValues: _.chain(entity.fields)
      .map((f) => `${val(f)}`)
      .join(`, `)
      .value(),
    FieldTestValuesEscaped: _.chain(entity.fields)
      .map((f) => `${val(f, "'")}`)
      .join(`, `)
      .value(),
    FieldTestValuesUpdated: _.chain(entity.fields)
      .map((f) => `${valUpdated(f)}`)
      .join(`, `)
      .value(),
    FieldNameList: _.chain(entity.fields)
      .map((f) => f.name)
      .value(),
  };
}

module.exports = {
  properties,
  fromRow,
  toRow,
};
