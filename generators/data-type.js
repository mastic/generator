"use strict";

class FieldVisitor {
  acceptEnum(_field) {
    throw new Error("Method undefined acceptEnum");
  }

  acceptString(_field) {
    throw new Error("Method undefined acceptString");
  }

  acceptBoolean(_field) {
    throw new Error("Method undefined acceptBoolean");
  }

  acceptLong(_field) {
    throw new Error("Method undefined acceptLong");
  }

  acceptInteger(_field) {
    throw new Error("Method undefined acceptInteger");
  }

  acceptDouble(_field) {
    throw new Error("Method undefined acceptDouble");
  }

  acceptDatetime(_field) {
    throw new Error("Method undefined acceptDatetime");
  }

  acceptRelation(_field) {
    throw new Error("Method undefined acceptRelation");
  }

  // eslint-disable-next-line complexity
  visit(field) {
    if (!field) {
      throw new Error(`undefined field`);
    }

    if (field.isEnum) {
      if (field.isEnum()) return this.acceptEnum(field);
    }

    if (field.isRelation) {
      if (field.isRelation()) return this.acceptRelation(field);
    }

    if (field.type) {
      switch (field.type) {
        case "String":
        case "string":
        case "str":
          return this.acceptString(field);

        case "bool":
        case "boolean":
        case "Boolean":
          return this.acceptBoolean(field);

        case "number":
        case "int":
        case "Integer":
        case "integer":
          return this.acceptInteger(field);

        case "long":
        case "Long":
          return this.acceptLong(field);

        case "double":
        case "Double":
        case "float":
        case "Float":
          return this.acceptDouble(field);

        case "datetime":
        case "DateTime":
        case "Datetime":
          return this.acceptDatetime(field);

        default:
          throw new Error(`unknown field type for ${JSON.stringify(field)}`);
      }
    }

    throw new Error(`undefined field type for ${JSON.stringify(field)}`);
  }
}

module.exports = {
  FieldVisitor,
};
