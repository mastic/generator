"use strict";
const BaseGenerator = require("../base-generator");

module.exports = class extends BaseGenerator {
  writing() {
    this.copy(["src/main/resources/META-INF/resources/index.html"]);
    const packageDir = this.application.packageFolder;
    this.template(
      "src/main/java/package/ExampleResource.java.ejs",
      `src/main/java/${packageDir}/ExampleResource.java`
    );
    this.template(
      "src/test/java/package/ExampleResourceTest.java.ejs",
      `src/test/java/${packageDir}/ExampleResourceTest.java`
    );
    this.template(
      "src/native-test/java/package/NativeExampleResourceIT.java.ejs",
      `src/native-test/java/${packageDir}/NativeExampleResourceIT.java`
    );
  }
};
