"use strict";
const BaseGenerator = require("../base-generator");
const pin = require("../pin-utils");
const inject = pin.injectContentToFile;

function injectIntoGitlabCI(fs, pin, content) {
  const dir = process.cwd();
  const file = ".gitlab-ci.yml";
  return inject(fs, dir, file, pin, content);
}

module.exports = class extends BaseGenerator {
  constructor(args, opts) {
    super(args, opts);
    this.staticFiles = [".dockerignore", "Dockerfile", "nginx.conf"];
  }

  writing() {
    this.copy(this.staticFiles);
    injectIntoGitlabCI(
      this.fs,
      `mastic-pin-macros`,
      `
.kaniko:
  stage: publish
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  artifacts:
    untracked: true
    when: always
    expire_in: 1 week
  before_script:
    - 'echo "Publishing \${CI_PROJECT_NAME} \${CI_COMMIT_TAG}"'
    - 'export HOME=/kaniko'
    - |
        cat <<EOF > /kaniko/.docker/config.json
        {
            "auths": {
                "$CI_REGISTRY": {
                    "username": "$CI_REGISTRY_USER",
                    "password": "$CI_REGISTRY_PASSWORD"
                }
            }
        }
        EOF
    - 'cat /kaniko/.docker/config.json'
  only:
    - master
  script:
    - /kaniko/executor
        --context \${CI_PROJECT_DIR}
        --dockerfile=\${CI_PROJECT_DIR}/Dockerfile
        --build-arg app=angular-stack-sample
        --destination \${CI_REGISTRY_IMAGE}:\${VERSION}

stages:
  - build
  - test
  - publish
`
    );
    injectIntoGitlabCI(
      this.fs,
      `mastic-pin-jobs`,
      `publish:master:
  extends: .kaniko
  variables:
    VERSION: snapshot

publish:branch:
  extends: .kaniko
  when: manual
  only:
    - branches
  except:
    - master
  variables:
    VERSION: dev-\${CI_COMMIT_REF_SLUG}
`
    );
  }
};
