"use strict";
const yaml = require("./yaml-utils");
const { OneToOne, OneToMany, ManyToOne, ManyToMany } = require("./relations");

const relationTypes = [OneToOne, OneToMany, ManyToOne, ManyToMany];

function readModel(fs, filename) {
  const model = yaml.readYaml(fs, filename);
  if (model && model.entities) {
    const enums = model.enums ? model.enums.map((en) => en.name) : [];
    const entities = model.entities.map((en) => en.name);
    model.entities
      .map((entity) => entity.fields)
      .forEach((fields) => {
        fields.forEach((field) => {
          field.accept = (visitor) => {
            if (visitor && visitor.visit) return visitor.visit(field);
            throw new Error(
              `cannot accept a non-visitor parameter: ${visitor}`
            );
          };

          field.isEnum = () => enums.includes(field.type);
          field.getEnum = () => {
            if (field.isEnum()) {
              return model.enums.filter((e) => e.name === field.type).pop();
            }
          };

          field.enum = field.getEnum();

          field.isRelation = () =>
            field.relation !== undefined && entities.includes(field.type);
          field.getRelation = () => {
            if (field.isRelation()) {
              if (relationTypes.includes(field.relation)) return field.relation;
              throw new Error(
                `relation should be one of ${relationTypes} but was ${field.relation}`
              );
            }
          };

          field.relation = field.getRelation();

          field.getTypeEntity = () => {
            if (field.isRelation()) {
              return model.entities.find((e) => field.type === e.name);
            }
          };

          field.typeEntity = field.getTypeEntity();

          field.isOptional = () =>
            field.optional !== undefined && field.optional;
        });
      });
  }

  return model;
}

function writeModel(fs, filename, model) {
  yaml.writeYaml(fs, filename, model);
}

module.exports = {
  readModel,
  writeModel,
};
