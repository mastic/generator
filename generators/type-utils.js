"use strict";

const _ = require("lodash");
const Chance = require("chance");
const { FieldVisitor } = require("./data-type");

class TestValue extends FieldVisitor {
  constructor(updated, stringDelimiter = `"`) {
    super();
    this.stringDelimiter = stringDelimiter;
    this.updated = updated;
    this.chance = new Chance(Math.random());
  }

  acceptEnum(field) {
    return `${field.type}.values()[0]`;
  }

  acceptString(_field) {
    const str = this.chance.animal();
    return this.updated
      ? `${this.stringDelimiter}${str} Updated${this.stringDelimiter}`
      : `${this.stringDelimiter}${str}${this.stringDelimiter}`;
  }

  acceptBoolean(_field) {
    const bool = this.chance.bool();
    return this.updated ? !bool : bool;
  }

  acceptLong(_field) {
    const i = this.chance.integer({ min: 0, max: 32767 });
    return `${this.updated ? i + 42 : i}L`;
  }

  acceptInteger(_field) {
    const i = this.chance.integer({ min: 0, max: 32767 });
    return this.updated ? i + 42 : i;
  }

  acceptDouble(_field) {
    const f = this.chance.floating();
    return this.updated ? f + 42 : f;
  }

  acceptDatetime(_field) {
    return this.updated
      ? `Instant.parse("2020-02-02T10:00:00.00Z")`
      : `Instant.parse("2020-02-01T10:00:00.00Z")`;
  }

  acceptRelation(field) {
    return `data.get${field.type}().getId()`;
  }
}

function testValue(field, stringDelimiter) {
  return field.accept(new TestValue(false, stringDelimiter));
}

function testValueUpdated(field, stringDelimiter) {
  return field.accept(new TestValue(true, stringDelimiter));
}

class DatabaseType extends FieldVisitor {
  acceptEnum(_field) {
    return "VARCHAR";
  }

  acceptString(field) {
    const notNull = field.isOptional() ? `` : ` NOT NULL`;
    return `VARCHAR${notNull}`;
  }

  acceptBoolean(_field) {
    return `BOOLEAN`;
  }

  acceptLong(field) {
    const notNull = field.isOptional() ? `` : ` NOT NULL`;
    return `INTEGER${notNull}`;
  }

  acceptInteger(field) {
    const notNull = field.isOptional() ? `` : ` NOT NULL`;
    return `INTEGER${notNull}`;
  }

  acceptDouble(field) {
    const notNull = field.isOptional() ? `` : ` NOT NULL`;
    return `DOUBLE PRECISION${notNull}`;
  }

  acceptDatetime(field) {
    const notNull = field.isOptional() ? `` : ` NOT NULL`;
    return `TIMESTAMP${notNull}`;
  }

  acceptRelation(field) {
    const notNull = field.isOptional() ? `` : ` NOT NULL`;
    return `INTEGER${notNull}`;
  }
}

function dbType(field) {
  return field.accept(new DatabaseType());
}

class JavaType extends FieldVisitor {
  acceptEnum(field) {
    return field.type;
  }

  acceptString(_field) {
    return `String`;
  }

  acceptBoolean(_field) {
    return `Boolean`;
  }

  acceptLong(_field) {
    return "Long";
  }

  acceptInteger(_field) {
    return "Integer";
  }

  acceptDouble(_field) {
    return `Double`;
  }

  acceptDatetime(_field) {
    return `Instant`;
  }

  acceptRelation(field) {
    return field.type;
  }
}

function javaType(field) {
  return field.accept(new JavaType());
}

class JavaImports extends FieldVisitor {
  acceptEnum(_field) {
    return [];
  }

  acceptString(field) {
    return field.isOptional() ? [`org.springframework.lang.Nullable`] : [];
  }

  acceptBoolean(field) {
    return field.isOptional() ? [`org.springframework.lang.Nullable`] : [];
  }

  acceptLong(field) {
    return field.isOptional() ? [`org.springframework.lang.Nullable`] : [];
  }

  acceptInteger(field) {
    return field.isOptional() ? [`org.springframework.lang.Nullable`] : [];
  }

  acceptDouble(field) {
    return field.isOptional() ? [`org.springframework.lang.Nullable`] : [];
  }

  acceptDatetime(field) {
    return field.isOptional()
      ? [`org.springframework.lang.Nullable`, `java.time.Instant`]
      : [`java.time.Instant`];
  }

  acceptRelation(field) {
    return field.isOptional() ? [`org.springframework.lang.Nullable`] : [];
  }
}

function javaImports(fields) {
  if (!Array.isArray(fields)) fields = [fields];

  return _.chain(fields)
    .filter((field) => field && field.accept)
    .map((field) => field.accept(new JavaImports()))
    .flatMapDeep()
    .sortedUniq()
    .map((t) => `import ${t};`)
    .join(`\n`)
    .value();
}

module.exports = {
  testValue,
  testValueUpdated,
  javaType,
  javaImports,
  dbType,
};
