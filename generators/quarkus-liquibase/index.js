"use strict";
const _ = require("lodash");
const BaseGenerator = require("../base-generator");
const str = require("../string-utils");
const pin = require("../pin-utils");
const { dbType } = require("../type-utils");
const props = require("../quarkus/entity-utils").properties;

const cap = str.camelCapitalize;
const fieldName = str.snakeUncapitalize;
const { addGradleDependency, addApplicationProperty, addEntityChangeLog } = pin;

module.exports = class extends BaseGenerator {
  writing() {
    addGradleDependency(
      this.fs,
      "implementation",
      "io.quarkus:quarkus-liquibase"
    );
    addGradleDependency(
      this.fs,
      "implementation",
      "io.quarkus:quarkus-jdbc-postgresql"
    );

    addApplicationProperty(
      this.fs,
      "quarkus.datasource.jdbc.url",
      `jdbc:postgresql://postgresdb:5432/${this.application.app.name}`
    );
    addApplicationProperty(
      this.fs,
      "quarkus.liquibase.migrate-at-start",
      "true"
    );

    this.template("src/main/resources/db/changeLog.xml.ejs");
    this.template("src/main/resources/db/initial_schema.xml.ejs");

    if (this.model && this.model.entities) {
      this.model.entities.forEach((entity) => {
        const entityProps = props(entity, this.application);
        this.template(
          "src/main/resources/db/entity_Entity.xml.ejs",
          `src/main/resources/db/entity_${cap(entity.name)}.xml`,
          {
            ...entityProps,
            LiquibaseColumns: _.chain(entity.fields)
              .map(
                (f) =>
                  `<column name="${fieldName(f.name)}" type="${dbType(
                    f
                  )}"><constraints nullable="false" /></column>`
              )
              .join(`\n      `)
              .value(),
          }
        );
        addEntityChangeLog(this.fs, cap(entity.name));
      });
    }
  }
};
