"use strict";
const _ = require("lodash");
const Chance = require("chance");
const pad = require("pad");
const { pluralize } = require("inflected");
const str = require("../string-utils");
const { FieldVisitor } = require("../data-type");
const { endpoint } = require("../string-utils");

const cap = str.camelCapitalize;
const uncap = str.camelUncapitalize;
const endpoints = str.pluralEndpoint;
const tableName = str.snakeCapitalize;
const { title, pluralTitle } = str;

class TestValue extends FieldVisitor {
  constructor(updated, stringDelimiter = `'`) {
    super();
    this.stringDelimiter = stringDelimiter;
    this.updated = updated;
    this.chance = new Chance(Math.random());
  }

  acceptEnum(_field) {
    return `0`;
  }

  acceptString(_field) {
    const str = this.chance.animal();
    return this.updated
      ? `${this.stringDelimiter}${str} Updated${this.stringDelimiter}`
      : `${this.stringDelimiter}${str}${this.stringDelimiter}`;
  }

  acceptBoolean(_field) {
    const bool = this.chance.bool();
    return this.updated ? !bool : bool;
  }

  acceptLong(_field) {
    const i = this.chance.integer({ min: 0, max: 32767 });
    return `${this.updated ? i + 42 : i}`;
  }

  acceptInteger(_field) {
    const i = this.chance.integer({ min: 0, max: 32767 });
    return this.updated ? i + 42 : i;
  }

  acceptDouble(_field) {
    const f = this.chance.floating();
    return this.updated ? f + 42 : f;
  }

  acceptDatetime(_field) {
    return this.updated
      ? `new Date(Date.UTC(2020, 1, 1, 9, 0))`
      : `new Date(Date.UTC(2020, 1, 0, 9, 0))`;
  }

  acceptRelation(field) {
    return `data.get${field.type}().getId()`;
  }
}

function testValue(field, updated = false) {
  return field.accept(new TestValue(updated));
}

class TestValueAsString extends TestValue {
  constructor(updated, stringDelimiter = `'`) {
    super(updated, stringDelimiter);
  }

  acceptDatetime(_field) {
    return this.updated
      ? `new Date(Date.UTC(2020, 1, 1, 9, 0)).toISOString().split('T')[0]`
      : `new Date(Date.UTC(2020, 1, 0, 9, 0)).toISOString().split('T')[0]`;
  }
}

function testValueAsString(field, updated = false) {
  return field.accept(new TestValueAsString(updated));
}

class FieldType extends FieldVisitor {
  acceptEnum(field) {
    return field.type;
  }

  acceptString(_field) {
    return `string`;
  }

  acceptBoolean(_field) {
    return `boolean`;
  }

  acceptLong(_field) {
    return "number";
  }

  acceptInteger(_field) {
    return "number";
  }

  acceptDouble(_field) {
    return `number`;
  }

  acceptDatetime(_field) {
    return `Date`;
  }

  acceptRelation(_field) {
    return `Long`;
  }
}

function fieldType(field) {
  return field.accept(new FieldType());
}

function fieldName(field) {
  return field.isRelation() ? uncap(`${field.name}Id`) : uncap(field.name);
}

function fieldTitle(field) {
  return title(field.name);
}

class FormField extends FieldVisitor {
  wrapFormField(field, input) {
    const requiredError = field.isOptional()
      ? ``
      : `
        <mat-error *ngIf="${fieldName(field)}.invalid">
          <div *ngIf="${fieldName(field)}.errors!['required']">
            ${fieldTitle(field)} is required.
          </div>
        </mat-error>`;
    return `<mat-form-field appearance="fill">
        <mat-label>${fieldTitle(field)}</mat-label>
        ${input}${requiredError}
      </mat-form-field>`;
  }

  acceptEnum(field) {
    const req = field.isOptional() ? "" : " required";
    const options = _.chain(field.enum.values)
      .map((v) => `<mat-option value="${v}">${v}</mat-option>`)
      .join(`\n          `)
      .value();
    const input = `<mat-select id="${fieldName(
      field
    )}" [formControl]="${fieldName(field)}"${req} >
          ${options}
        </mat-select>`;
    return this.wrapFormField(field, input);
  }

  acceptString(field) {
    const req = field.isOptional() ? "" : " required";
    const input = `<input matInput placeholder="Some ${fieldTitle(
      field
    )}" id="${fieldName(field)}" [formControl]="${fieldName(field)}"${req} />`;
    return this.wrapFormField(field, input);
  }

  acceptBoolean(field) {
    return `<mat-slide-toggle [formControl]="${fieldName(
      field
    )}" id="${fieldName(field)}">${fieldTitle(field)}</mat-slide-toggle>`;
  }

  acceptLong(field) {
    const req = field.isOptional() ? "" : " required";
    const input = `<input matInput type="number" placeholder="Some ${fieldTitle(
      field
    )}" id="${fieldName(field)}" [formControl]="${fieldName(field)}"${req} />`;
    return this.wrapFormField(field, input);
  }

  acceptInteger(field) {
    return this.acceptLong(field);
  }

  acceptDouble(field) {
    return this.acceptLong(field);
  }

  acceptDatetime(field) {
    const req = field.isOptional() ? "" : " required";
    const input = `<input type="date" matInput placeholder="Some ${fieldTitle(
      field
    )}" id="${fieldName(field)}" [formControl]="${fieldName(field)}"${req} />`;
    return this.wrapFormField(field, input);
  }

  acceptRelation(_field) {
    return `relations are not supported yet`;
  }
}

function formField(field) {
  return field.accept(new FormField());
}

class FillFormField extends FieldVisitor {
  acceptEnum(field) {
    return `component.${fieldName(field)}.setValue(0);`;
  }

  acceptString(field) {
    return `const ${fieldName(
      field
    )} = fixture.nativeElement.querySelector('#${fieldName(field)}');
    ${fieldName(field)}.value = ${testValueAsString(field)};
    ${fieldName(field)}.dispatchEvent(event);`;
  }

  acceptBoolean(field) {
    return this.acceptString(field);
  }

  acceptLong(field) {
    return this.acceptString(field);
  }

  acceptInteger(field) {
    return this.acceptString(field);
  }

  acceptDouble(field) {
    return this.acceptString(field);
  }

  acceptDatetime(field) {
    return this.acceptString(field);
  }

  acceptRelation(field) {
    return this.acceptString(field);
  }
}

function fillFormField(field) {
  return field.accept(new FillFormField());
}

function testObjects(
  entity,
  { count = 1, indent = 2, initial = 0, updated = false, partial = false }
) {
  const padding = pad(` `, indent);
  const initPadding = pad(``, initial);
  return _.chain(_.range(1, count + 1))
    .map((n) => {
      const fields = _.chain(entity.fields)
        .filter((f) => !f.isRelation())
        .map((f) => `${padding}${fieldName(f)}: ${testValue(f, updated)},`)
        .join(`\n  `)
        .value();
      const prefix = partial ? `` : `  ${padding}id: '${n}',\n`;
      return `${initPadding}{\n${prefix}  ${fields}\n${padding}}`;
    })
    .join(`,\n`)
    .value();
}

function properties(entity, properties) {
  if (!properties) {
    properties = {};
  }

  return {
    ...properties,
    ClassName: cap(entity.name),
    ClassNames: pluralize(cap(entity.name)),
    TableName: tableName(entity.name),
    ObjectName: uncap(entity.name),
    ObjectNames: pluralize(uncap(entity.name)),
    Route: endpoint(entity.name),
    Routes: endpoints(entity.name),
    EndPoint: endpoints(entity.name),
    EntityTitle: title(entity.name),
    EntityTitles: pluralTitle(entity.name),
    Fields: _.chain(entity.fields)
      .filter((f) => !f.isRelation())
      .map((f) => {
        const opt = f.isOptional() ? "?" : "";
        return `${fieldName(f)}${opt}: ${fieldType(f)};`;
      })
      .join(`\n  `)
      .value(),
    FieldColumns: _.chain(entity.fields)
      .filter((f) => !f.isRelation())
      .map((f) => `'${fieldName(f)}',`)
      .join(`\n    `)
      .value(),
    FieldTableColumns: _.chain(entity.fields)
      .filter((f) => !f.isRelation())
      .map(
        (f) => `<!-- ${fieldName(f)} Column -->
  <ng-container matColumnDef="${fieldName(f)}">
    <th mat-header-cell class="dc-label" *matHeaderCellDef>${fieldTitle(f)}</th>
    <td mat-cell class="dc-content" *matCellDef="let element">{{ element.${fieldName(
      f
    )} }}</td>
  </ng-container>\n`
      )
      .join(`\n  `)
      .value(),
    FieldAssignments: _.chain(entity.fields)
      .filter((f) => !f.isRelation())
      .map((f) => `${fieldName(f)}: entity.${fieldName(f)}!,`)
      .join(`\n      `)
      .value(),
    FormFieldInitializers: _.chain(entity.fields)
      .filter((f) => !f.isRelation())
      .map((f) => `${fieldName(f)}: new FormControl(''),`)
      .join(`\n    `)
      .value(),
    FormFieldGetters: _.chain(entity.fields)
      .filter((f) => !f.isRelation())
      .map(
        (f) => `get ${fieldName(f)}(): FormControl {
    return this.${uncap(entity.name)}Form.get('${fieldName(f)}') as FormControl;
  }`
      )
      .join(`\n\n  `)
      .value(),
    FormFields: _.chain(entity.fields)
      .filter((f) => !f.isRelation())
      .map((f) => formField(f))
      .join(`\n\n      `)
      .value(),
    TestEntities: _.chain(_.range(1, 4))
      .map((n) => {
        const fields = _.chain(entity.fields)
          .filter((f) => !f.isRelation())
          .map((f) => `   ${fieldName(f)}: ${testValue(f)},`)
          .join(`\n        `)
          .value();
        return `${n}: { id: '${n}',\n        ${fields}\n      },`;
      })
      .join(`\n      `)
      .value(),
    TestObject: testObjects(entity, { indent: 6 }),
    TestObject4Indent: testObjects(entity, { indent: 4 }),
    TestObject12Indent: testObjects(entity, { indent: 12 }),
    TestObjects2: testObjects(entity, { count: 2, indent: 8, initial: 8 }),
    TestObjects3: testObjects(entity, { count: 3, indent: 6, initial: 6 }),
    TestObjectUpdated: testObjects(entity, { indent: 6, updated: true }),
    TestObjectUpdatedPartial: testObjects(entity, {
      indent: 6,
      updated: true,
      partial: true,
    }),
    FillFormFields: _.chain(entity.fields)
      .filter((f) => !f.isRelation())
      .map((f) => fillFormField(f))
      .join(`\n\n    `)
      .value(),
    CheckFormFields: _.chain(entity.fields)
      .filter((f) => !f.isRelation())
      .map(
        (f) =>
          `it('should have ${fieldName(
            f
          )}', () => {\n    expect(component.${fieldName(
            f
          )}).toBeTruthy();\n  });`
      )
      .join(`\n\n  `)
      .value(),
  };
}

module.exports = {
  properties,
};
