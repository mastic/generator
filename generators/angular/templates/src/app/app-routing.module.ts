import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// mastic-pin-import-statements

const routes: Routes = [
  // mastic-pin-routing-mid-route
  // mastic-pin-routing-last-route
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
