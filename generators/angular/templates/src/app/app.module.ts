import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';

// mastic-pin-app-import-statements

@NgModule({
  declarations: [
    AppComponent,
    // mastic-pin-app-declarations
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SharedModule,
    // mastic-pin-app-imports
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
