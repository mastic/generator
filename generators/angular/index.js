"use strict";
const BaseGenerator = require("../base-generator");
const str = require("../string-utils");
const cap = str.camelCapitalize;
const { title } = str;

module.exports = class extends BaseGenerator {
  constructor(args, opts) {
    super(args, opts);
    this.staticFiles = [
      ".browserslistrc",
      ".editorconfig",
      ".eslintrc.json",
      ".gitignore",
      "proxy.conf.json",
      "src/app/app.module.ts",
      "src/app/app-routing.module.ts",
      "src/assets/.gitkeep",
      "src/environments/environment.prod.ts",
      "src/environments/environment.ts",
      "src/favicon.ico",
      "src/main.ts",
      "src/polyfills.ts",
      "src/styles.sass",
      "tsconfig.app.json",
      "tsconfig.json",
      "tsconfig.spec.json",
    ];
  }

  writing() {
    this.copy(this.staticFiles);

    const props = {
      ...this.application,
      AppName: cap(this.application.app.name),
      AppTitle: title(this.application.app.name),
    };
    this.template("angular.json.ejs");
    this.template("karma.conf.js.ejs");
    this.template("package.json.ejs");
    this.template("README.md.ejs", "README.md", props);
    this.template("src/test.ts.ejs");
    this.template("src/index.html.ejs", "src/index.html", props);
    this.template("src/app/app.component.html.ejs");
    this.template("src/app/app.component.sass.ejs");
    this.template("src/app/app.component.spec.ts.ejs");
    this.template("src/app/app.component.ts.ejs");
    this.template("src/app/shared/shared.module.ts.ejs");
  }
};
