"use strict";
const _ = require("lodash");
const yaml = require("js-yaml");

function readYaml(fs, filename) {
  try {
    const content = fs.readFileSync
      ? fs.readFileSync(filename)
      : fs.read(filename);
    return yaml.safeLoad(content);
  } catch (_) {
    return undefined;
  }
}

function writeYaml(fs, filename, object, log) {
  if (!fs || !filename) return undefined;
  if (!object) object = {};

  try {
    const content = yaml.dump(object);
    if (fs.writeFileSync) {
      fs.writeFileSync(filename, content);
    } else {
      fs.write(filename, content);
    }
  } catch (error) {
    if (!log) log = console.log;
    log(`Could not write to file ${filename}: ${error}`);
  }
}

function merge(current, content, path) {
  if (!(Array.isArray(path) && path.length)) {
    return _.merge(current, content);
  }

  const branchName = path[0];
  const pathRest = path.slice(1);
  let branchValue = { ...current[branchName] };
  branchValue = merge(branchValue, content, pathRest);
  return { ...current, [branchName]: branchValue };
}

// eslint-disable-next-line max-params
function injectYaml(fs, filename, yamlPath, content, log) {
  if (content) {
    let path = [];
    let current = readYaml(fs, filename, log);
    if (yamlPath) path = yamlPath.split(".");
    if (!current) current = {};
    const result = merge(current, content, path);
    writeYaml(fs, filename, result, log);
  }
}

module.exports = {
  readYaml,
  writeYaml,
  injectYaml,
};
