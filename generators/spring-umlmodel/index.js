"use strict";
const _ = require("lodash");
const BaseGenerator = require("../base-generator");
const { OneToOne, ManyToOne } = require("../relations");
const inject = require("../pin-utils").injectContentToFile;

module.exports = class extends BaseGenerator {
  properties() {
    if (this.model && this.model.entities) {
      this.application.UMLEntities = _.chain(this.model.entities)
        .map((e) => this._entity(e))
        .join(`\n`)
        .value();
    } else {
      this.application.UMLEntities = "";
    }

    if (this.model && this.model.enums) {
      this.application.UMLEnums = _.chain(this.model.enums)
        .map((e) => this._enum(e))
        .join(`\n`)
        .value();
    } else {
      this.application.UMLEnums = "";
    }
  }

  _entity(entity) {
    const fields = _.chain(entity.fields)
      .map((f) => {
        const opt = f.isOptional() ? "?" : "";
        return `  + ${f.type} ${f.name}${opt}\n`;
      })
      .join(``)
      .value();
    const relations = _.chain(entity.fields)
      .filter(
        (f) => f.getRelation() === ManyToOne || f.getRelation() === OneToOne
      )
      .map((f) => `${entity.name} "*" *--o "1" ${f.type} : ${f.name}`)
      .join(`\n`)
      .value();
    const enums = _.chain(entity.fields)
      .filter((f) => f.isEnum())
      .map((f) => `${entity.name} --> ${f.type} : ${f.name}`)
      .join(`\n`)
      .value();
    return `class ${entity.name} {\n${fields}}\n${relations}\n${enums}\n`;
  }

  _enum(enm) {
    const fields = _.chain(enm.values)
      .map((val) => `  - ${val}\n`)
      .join(``)
      .value();
    return `enum ${enm.name} {\n${fields}}\n`;
  }

  writing() {
    this.template("model.puml.ejs");
    const plantuml = `plantuml\n\n${this.application.UMLEntities}\n${this.application.UMLEnums}\n`;
    const dir = process.cwd();
    const pin = "mastic-pin-readme";
    const content = `## Model\n\n\`\`\`${plantuml}\`\`\`\n`;
    inject(this.fs, dir, "README.md", pin, content);
  }
};
