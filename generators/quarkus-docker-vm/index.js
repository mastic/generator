"use strict";
const BaseGenerator = require("../base-generator");

module.exports = class extends BaseGenerator {
  writing() {
    this.template("src/main/docker/Dockerfile.jvm.ejs");
  }
};
