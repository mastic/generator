# generator-mastic [![NPM version][npm-image]][npm-url] [![License](https://img.shields.io/badge/license-MIT-green)](https://gitlab.com/restbase/generator/-/blob/master/LICENSE) [![Build Status](https://gitlab.com/mastic/generator/badges/master/pipeline.svg)](https://gitlab.com/mastic/generator/pipelines) [![Coverage percentage](https://gitlab.com/mastic/generator/badges/master/coverage.svg?style=flat)](https://gitlab.com/mastic/generator/-/pipelines)
> Mastic Yeoman generator

This is the generator for the mastic project. Please refer to the documentation at https://mastic.gitlab.io/ for further details.

## Installation

First, install [Yeoman](http://yeoman.io) and generator-mastic using [npm](https://www.npmjs.com/) (we assume you have pre-installed [node.js](https://nodejs.org/)).

```bash
npm install -g yo generator-mastic
```

Then generate your new project:

```bash
mastic
```

## License

MIT © [mastic developers](https://mastic.gitlab.io/)


[npm-image]: https://badge.fury.io/js/generator-mastic.svg
[npm-url]: https://npmjs.org/package/generator-mastic
