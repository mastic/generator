"use strict";
const files = require("fs");
const fg = require("fast-glob");
const parse = require("parse-gitignore");
const modelUtils = require("../generators/model-utils");
const { readYaml, writeYaml } = require("../generators/yaml-utils");

const DEFAULT_APPLICATION = require("./default-application");
const DEFAULT_STACK = require("./default-stack");
const DEFAULT_IGNORES = [];
const DEFAULT_MODEL = {};

const APPLICATION_FILE = "application.yml";
const STACK_FILE = "stack.yml";
const MODEL_FILE = "model.yml";
const IGNORES_FILE = ".masticignore";

const readModel = (fs) => {
  let model = modelUtils.readModel(fs, MODEL_FILE);
  if (!model) {
    model = DEFAULT_MODEL;
  }

  return model;
};

const readStack = (fs) => {
  const stack = readYaml(fs, STACK_FILE);
  if (!stack) {
    return DEFAULT_STACK;
  }

  return stack;
};

const readIgnores = (fs) => {
  const filename = IGNORES_FILE;
  try {
    const content = fs.readFileSync
      ? fs.readFileSync(filename)
      : fs.read(filename);
    return parse(content) || DEFAULT_IGNORES;
  } catch (_) {
    return DEFAULT_IGNORES;
  }
};

const readApplication = (fs) => {
  let properties = readYaml(fs, APPLICATION_FILE);
  if (!properties) {
    properties = DEFAULT_APPLICATION;
  }

  const pkg = properties.app.package;
  properties.packageFolder = pkg ? pkg.replace(/\./g, "/") : "";
  properties.MasticVersion = "0.2.0";

  return properties;
};

const readConfiguration = (fs) => {
  if (!fs) fs = files;

  return {
    application: readApplication(fs),
    stack: readStack(fs),
    model: readModel(fs),
    ignores: fg.sync(readIgnores(fs), { dot: true }),
  };
};

const writeConfiguration = (fs, log, config) => {
  if (!fs) fs = files;
  if (!log) log = console.log;
  writeYaml(fs, APPLICATION_FILE, config.application, log);
  writeYaml(fs, STACK_FILE, config.stack, log);
  writeYaml(fs, MODEL_FILE, config.model, log);
  writeYaml(fs, IGNORES_FILE, config.ignores, log);
};

module.exports = {
  readConfiguration,
  writeConfiguration,
};
