"use strict";

const chalk = require("chalk");
const figures = require("figures");
const yosay = require("yosay");

const { readConfiguration } = require("./configuration");

const printWelcome = (stackName) => {
  console.log(
    yosay(`Welcome to the ${chalk.red("mastic")} generator! ${figures.smiley}`)
  );
  console.log(`Stack: ${chalk.blueBright(stackName)}`);
};

const defineGenerators = (stack) => {
  const stackName = stack.name;
  const generators = [[`../generators/${stackName}`, `mastic:${stackName}`]];
  if (stack.aspects) {
    console.log(`Aspects:`);
    stack.aspects.forEach((aspect) => {
      console.log(
        `  ${chalk.blueBright(figures.dot)} ${chalk.blueBright(aspect)}`
      );
      generators.push([
        `../generators/${stackName}-${aspect}`,
        `mastic:${stackName}-${aspect}`,
      ]);
    });
  }

  return generators;
};

const generateWith = (generators, args, configuration) => {
  const yeoman = require("yeoman-environment");
  const env = yeoman.createEnv();

  env.on("error", (err) => {
    console.error("Error", process.argv.slice(2).join(" "), "\n");
    console.error(args.debug ? err.stack : err.message);
    process.exit(err.code || 1);
  });

  generators.forEach(([genPath, genName]) => {
    try {
      env.register(require.resolve(genPath), genName);
      console.log(
        ` ${chalk.greenBright(
          figures.arrowRight
        )}  running ${chalk.yellowBright(genName)}...`
      );
      env.run(genName, configuration, (err) => {
        if (err) {
          console.log(chalk.red(`Error occured: ${JSON.stringify(err)}`));
          console.log(err.stack);
          process.exit(err.code || 1);
        } else {
          console.log(
            ` ${chalk.greenBright(figures.tick)}  ${chalk.yellowBright(
              genName
            )} completed!`
          );
        }
      });
    } catch (err) {
      console.error(chalk.red(`Could not run ${genName}. Skipping... `));
      console.error(err.stack);
    }
  });
};

const generator = (args) => {
  const configuration = readConfiguration();
  const { stack } = configuration;

  printWelcome(stack.name);

  const generators = defineGenerators(stack);
  generateWith(generators, args, configuration);
};

module.exports = {
  generator,
};
