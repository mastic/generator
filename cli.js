#!/usr/bin/env node

"use strict";
const yargs = require("yargs");
const chalk = require("chalk");
const REPO = require("./package.json").repository;
const os = require("os");

try {
  yargs
    .scriptName("mastic")
    .commandDir("commands")
    .strict(true)
    .help(true)
    .showHelpOnFail(true)
    .wrap(yargs.terminalWidth())
    .parse();
} catch (error) {
  if (error) {
    console.error(
      chalk.red("An unexpected error occurred! Please open an issue at:")
    );
    console.error(chalk.green(`${REPO}/issues`));
    console.error(chalk.red("Copy this error message into it:"));
    console.error(chalk.white("------------------------------------------"));
    console.error(chalk.white(`Node Version: ${process.version}`));
    console.error(chalk.white(`Platform: ${process.platform} ${os.release()}`));
    console.error(chalk.white(`Processor Architecture: ${process.arch}`));
    console.error(chalk.white(error));
    console.error(chalk.white("------------------------------------------"));
    console.error(error.stack);
    process.exit(1);
  }
}
