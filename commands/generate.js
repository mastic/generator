"use strict";

const chalk = require("chalk");
const mastic = require("../mastic/generator");

exports.command = "generate";
exports.aliases = ["generate", "gen", "$0"];
exports.description = chalk.yellow("Generates an application");
exports.handler = mastic.generator;
